
===============
Getting Started
===============

The app3js library is a collection of modules which contain specific functionality for the APIS ecosystem.

- The ``app3-apis`` is for the APIS blockchain and smart contracts
- The ``app3-utils`` contains useful helper functions for Dapp developers.


.. _adding-app3js:

Adding app3js
==============

.. index:: npm

First you need to get app3js into your project. This can be done using the following methods:

- npm: ``npm install app3js``

After that you need to create a app3js instance and set a provider.
You should connect to a remote/local node.

.. code-block:: javascript

    // in node.js use: var App3 = require('app3js');

    const wsProvider = new App3.providers.WebsocketProvider('ws://153a17085797541e1b821aa7f0:111d8060fd25b75dfadbe0379f@127.0.0.1:40405');

    // Disable AES encryption and improve communication speed with RPC.
    wsProvider.enableEncryption(false);

    var app3 = new App3(App3.givenProvider || wsProvider);


.. code-block:: javascript

    // in node.js use: var App3 = require('app3js');

    const httpProvider = new new App3.providers.HttpProvider('http://a6180fb19186e1e14a83cadad92d13c1:4bc974a374d9552a30faa2269e5dd3a6@127.0.0.1:47194');

    const app3 = new App3();

    app3.setProvider(httpProvider);

That's it! now you can use the ``app3`` object.