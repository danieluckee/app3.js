app3js - APIS JavaScript APIS
==============================

app3js is a collection of libraries which allow you to interact with a local or remote APIS node, using a WebSocket connection.

The following documentation will guide you through :ref:`installing and running app3js<adding-app3js>`, as well as providing a API reference documentation with examples.

.. toctree::
    :maxdepth: 2
    :caption: User Documentation

    getting-started
    callbacks-promises-events
    glossary


.. toctree::
    :maxdepth: 2
    :caption: APIS Reference

    app3
    app3-apis
    app3-apis-subscribe
    app3-apis-contract
    app3-apis-accounts
    app3-apis-personal
    app3-apis-abi
    app3-utins

.. toctree::
    :maxdepth: 2
    :caption: JSON-RPC

    rpc-apis