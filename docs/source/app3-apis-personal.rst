=================
app3.apis.personal
=================


The ``app3-apis-personal`` package allows you to interact with the APIS node's accounts.

.. note:: Many of these functions send sensitive information, like password. Never call these functions over a unsecured Websocket or HTTP provider, as your password will be sent in plain text!


.. code-block:: javascript

    var Personal = require('app3-apis-personal');

    var personal = new Personal('ws://some.local-or-remote.node:8546');


    // or using the app3 umbrella package

    var App3 = require('app3');
    var app3 = new App3('ws://some.local-or-remote.node:8546');

    // -> app3.apis.personal




newAccount
==========

.. code-block:: javascript

    app3.apis.personal.newAccount(password, [callback])

Create a new account on the node that App3js is connected to with its provider. The RPC method used is ``personal_newAccount``. It differs from :ref:`app3.apis.accounts.create() <apis-accounts-create>` where the key pair is created only on client and it’s up to the developer to manage it.

.. note:: Never call this function over a unsecured Websocket or HTTP provider, as your password will be send in plain text!

----------
Parameters
----------

1. ``password`` - ``String``: The password to encrypt this account with.

-------
Returns
-------

``Promise`` returns ``String``: The address of the newly created account.

-------
Example
-------

.. code-block:: javascript

    app3.apis.personal.newAccount('!@superpassword')
    .then(console.log);
    > '0x1234567891011121314151617181920212223456'

------------------------------------------------------------------------------


sign
=====================

.. code-block:: javascript

    app3.apis.personal.sign(dataToSign, address, password [, callback])

Signs data using a specific account.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.


----------
Parameters
----------


1. ``String`` - Data to sign. If String it will be converted using :ref:`app3.utils.utf8ToHex <utils-utf8tohex>`.
2. ``String`` - Address to sign data with.
3. ``String`` - The password of the account to sign data with.
4. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


-------
Returns
-------


``Promise`` returns ``String`` - The signature.


-------
Example
-------


.. code-block:: javascript

    app3.apis.personal.sign("Hello world", "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe", "test password!")
    .then(console.log);
    > "0x30755ed65396facf86c53e6217c52b4daebe72aa4941d89635409de4c9c7f9466d4e9aaec7977f05e923889b33c0d0dd27d7226b6e6f56ce737465c5cfd04be400"

    // the below is the same
    app3.apis.personal.sign(app3.utils.utf8ToHex("Hello world"), "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe", "test password!")
    .then(console.log);
    > "0x30755ed65396facf86c53e6217c52b4daebe72aa4941d89635409de4c9c7f9466d4e9aaec7977f05e923889b33c0d0dd27d7226b6e6f56ce737465c5cfd04be400"


------------------------------------------------------------------------------


ecRecover
=====================

.. code-block:: javascript

    app3.apis.personal.ecRecover(dataThatWasSigned, signature [, callback])

Recovers the account that signed the data.

----------
Parameters
----------


1. ``String`` - Data that was signed. If String it will be converted using :ref:`app3.utils.utf8ToHex <utils-utf8tohex>`.
2. ``String`` - The signature.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


-------
Returns
-------


``Promise`` returns ``String`` - The account.


-------
Example
-------


.. code-block:: javascript

    app3.apis.personal.ecRecover("Hello world", "0x30755ed65396facf86c53e6217c52b4daebe72aa4941d89635409de4c9c7f9466d4e9aaec7977f05e923889b33c0d0dd27d7226b6e6f56ce737465c5cfd04be400").then(console.log);
    > "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe"

------------------------------------------------------------------------------

signTransaction
=====================

.. code-block:: javascript

    app3.apis.personal.signTransaction(transaction, password [, callback])

Signs a transaction. This account needs to be unlocked.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.

----------
Parameters
----------


1. ``Object`` - The transaction data to sign :ref:`app3.apis.sendTransaction() <apis-sendtransaction>` for more.
2. ``String`` - The password of the ``from`` account, to sign the transaction with.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


-------
Returns
-------


``Promise`` returns ``Object`` - The RLP encoded transaction. The ``raw`` property can be used to send the transaction using :ref:`app3.apis.sendSignedTransaction <apis-sendsignedtransaction>`.


-------
Example
-------


.. code-block:: javascript

    app3.apis.personal.signTransaction({
        from: "0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01",
        gasPrice: "50000000000",
        gas: "200000",
        to: '0x3535353535353535353535353535353535353535',
        value: "1000000000000000000",
        data: ""
    }, 'MyPassword!').then(console.log);
    > {
        raw: '0xf87180850ba43b740083030d4094353535353535353535353535353535353535353580880de0b6b3a76400008025a00975caffb4cb5c6f9dfe97315d3c448926ee0d3be85a44c1006be525d38ef5a3a01b8c0d9ac65615823dbdc0dfd398f5807d0582f490b2b408224e4803d46c3b69018080',
        tx: {
            hash: '0x185da901f30171b573d06ffdf1cbe8aa7e624eb3d207e4c6cede5c28c0fd948c',
            nonce: '0x',
            from: '0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01',
            to: '0x3535353535353535353535353535353535353535',
            value: '1000000000000000000',
            valueAPIS: '1',
            gas: '200000',
            gasPrice: '50000000000',
            gasPriceAPIS: '0.00000005',
            feePaidAPIS: '0.01',
            data: '',
            r: '0x0975caffb4cb5c6f9dfe97315d3c448926ee0d3be85a44c1006be525d38ef5a3',
            s: '0x1b8c0d9ac65615823dbdc0dfd398f5807d0582f490b2b408224e4803d46c3b69',
            v: '0x1b'
        }
    }

------------------------------------------------------------------------------

sendTransaction
=====================

.. code-block:: javascript

    app3.apis.personal.sendTransaction(transactionOptions, password [, callback])

This method sends a transaction over the management API.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.

----------
Parameters
----------


1. ``Object`` - The transaction options
2. ``String`` - The password of the ``from`` account, to sign the transaction with.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


-------
Returns
-------


``Promise`` returns ``string`` - The transaction hash


-------
Example
-------


.. code-block:: javascript

    app3.apis.personal.sendTransaction({
        from: "0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01",
        gasPrice: "50000000000",
        gas: "200000",
        to: '0x3535353535353535353535353535353535353535',
        value: "1000000000000000000",
        data: ""
    }, 'MyPassword!').then(console.log);
    > '0x185da901f30171b573d06ffdf1cbe8aa7e624eb3d207e4c6cede5c28c0fd948c'

------------------------------------------------------------------------------

unlockAccount
=====================

.. code-block:: javascript

    app3.apis.personal.unlockAccount(address, password, unlockDuraction [, callback])

Unlocks the given account.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.

----------
Parameters
----------


1. ``address`` - ``String`` - The account address.
2. ``password`` - ``String`` - The password of the account.
3. ``unlockDuration`` - ``Number`` - The duration for the account to remain unlocked.
4. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


-------
Returns
-------


``Promise`` returns ``boolean`` - True if the account got unlocked successful otherwise false.


-------
Example
-------


.. code-block:: javascript

    app3.apis.personal.unlockAccount("0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe", "test password!", 600)
    .then(console.log('Account unlocked!'));
    > "Account unlocked!"

------------------------------------------------------------------------------

lockAccount
=====================

.. code-block:: javascript

    app3.apis.personal.lockAccount(address [, callback])

Locks the given account.

----------
Parameters
----------


1. ``address`` - ``String`` - The account address.
2. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


-------
Returns
-------


``Promise`` returns ``boolean`` - True if the account got locked successful otherwise false.


-------
Example
-------


.. code-block:: javascript

    app3.apis.personal.lockAccount("0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe")
    .then(console.log('Account locked!'));
    > "Account locked!"

------------------------------------------------------------------------------

getAccounts
=====================

.. code-block:: javascript

    app3.apis.personal.getAccounts([callback])

Returns a list of accounts the node controls by using the provider and calling the RPC method ``personal_listAccounts``. Using app3.apis.accounts.create() will not add accounts into this list. For that use app3.apis.personal.newAccount().

The results are the same as ``app3.apis.getAccounts()`` except that calls the RPC method ``apis_accounts``.

-------
Returns
-------


``Promise`` returns ``Array`` - An array of addresses controlled by node.


-------
Example
-------


.. code-block:: javascript

    app3.apis.personal.getAccounts()
    .then(console.log);
    > [ {
            address: 'ceffabff83caf9594d12e101e93c71a3aaea96ef',
            index: '0',
            aAPIS: '0',
            aMNR: '28008000000000',
            nonce: '2',
            APIS: '0',
            MNR: '0.000028008'
        },
        {
            address: 'b8ce9ab6943e0eced004cde8e3bbed6568b2fa01',
            index: '1',
            aAPIS: '5000000000000000000000',
            aMNR: '20000000000000000',
            nonce: '0',
            APIS: '5,000',
            MNR: '0.02'
        } ]

------------------------------------------------------------------------------

importRawKey
=====================

.. code-block:: javascript

    app3.apis.personal.importRawKey(privateKey, password)

Imports the given private key into the key store, encrypting it with the passphrase.

Returns the address of the new account.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.

----------
Parameters
----------


1. ``privateKey`` - ``String`` - An unencrypted private key (hex string).
2. ``password`` - ``String`` - The password of the account.


-------
Returns
-------


``Promise`` returns ``String`` - The address of the account.


-------
Example
-------


.. code-block:: javascript

    app3.apis.personal.importRawKey("cd3376bb711cb332ee3fb2ca04c6a8b9f70c316fcdf7a1f44ef4c7999483295e", "password1234")
    .then(console.log);
    > "0x8f337bf484b2fc75e4b0436645dcc226ee2ac531"

