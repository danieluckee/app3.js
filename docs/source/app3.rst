
====
App3
====

    Class

This's main class of anything related APIS.

.. code-block:: javascript

    var App3 = require('app3js');

    > App3.utils
    > App3.version
    > App3.providers
    > App3.modules

------------------------------------------------------------------------------

App3.modules
=====================

    Property of App3 class

.. code-block:: javascript

    App3.modules

Will return an object with the classes of all major sub modules, to be able to instantiate them manually.

-------
Returns
-------

``Object``: A list of modules:
    - ``Apis`` - ``Function``: the Apis module for interacting with the APIS network see :ref:`app3.apis <apis>` for more.
    - ``Net`` - ``Function``: the Net module for interacting with network properties see :ref:`app3.apis.net <apis-net>` for more.
    - ``Personal`` - ``Function``: the Personal module for interacting with the APIS accounts see :ref:`app3.apis.personal <personal>` for more.

-------
Example
-------

.. code-block:: javascript

    App3.modules
    > {
        Apis: Apis function(provider),
        Net: Net function(provider),
        Personal: Personal function(provider)
    }


------------------------------------------------------------------------------

app3 object
============

    The instance of App3

The app3js object is an umbrella package to house all APIS related modules.

.. code-block:: javascript

    var App3 = require('app3js');

    var app3 = new App3('ws://some.local-or-remote.node:8546');

    > app3.apis
    > app3.utils
    > app3.version


------------------------------------------------------------------------------

version
============

    Property of App3 class and instance of App3

.. code-block:: javascript

    App3.version
    app3.version

Contains the version of the ``app3`` container object.

-------
Returns
-------

``String``: The current version.

-------
Example
-------

.. code-block:: javascript

    app3.version;
    > "0.9.3-6"



------------------------------------------------------------------------------


utils
=====================

    Property of App3 class and instance of App3

.. code-block:: javascript

    App3.utils
    app3.utils

Utility functions are also exposes on the ``App3`` class object directly.

See :ref:`app3.utils <utils>` for more.


------------------------------------------------------------------------------


.. include:: include_package-core.rst


------------------------------------------------------------------------------