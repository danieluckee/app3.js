.. _apis:

.. include:: include_announcement.rst

========
app3.apis
========

The ``app3-apis`` package allows you to interact with an APIS blockchain and APIS smart contracts.


.. code-block:: javascript

    var Apis = require('app3-apis');
    var apis = new Apis('ws://some.local-or-remote.node:8546');


    // or using the app3 umbrella package

    var App3 = require('app3');
    var app3 = new App3('ws://some.local-or-remote.node:8546');

    // -> app3.apis


Note on checksum addresses
=======

All APIS addresses returned by functions of this package are returned as checksum addresses.
This means some letters are uppercase and some are lowercase.
Based on that it will calculate a checksum for the address and prove its correctness.
Incorrect checksum addresses will throw an error when passed into functions.
If you want to circumvent the checksum check you can make an address all lower- or uppercase.


-------
Example
-------

.. code-block:: javascript

    app3.apis.getAccounts(console.log);
    > ["0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe" ,"0x85F43D8a49eeB85d32Cf465507DD71d507100C1d"]


------------------------------------------------------------------------------


subscribe
=====================

For ``app3.apis.subscribe`` see the :ref:`Subscribe reference documentation <apis-subscribe>`


------------------------------------------------------------------------------


Contract
=====================

For ``app3.apis.Contract`` see the :ref:`Contract reference documentation <apis-contract>`


------------------------------------------------------------------------------


personal
=====================

For ``app3.apis.personal`` see the :ref:`personal reference documentation <apis-personal>`


------------------------------------------------------------------------------

accounts
=====================

For ``app3.apis.accounts`` see the :ref:`accounts reference documentation <apis-accounts>`


------------------------------------------------------------------------------

abi
=====================

For ``app3.apis.abi`` see the :ref:`ABI reference documentation <apis-abi>`


------------------------------------------------------------------------------


net
=====================

For ``app3.apis.net`` see the :ref:`net reference documentation <apis-net>`


------------------------------------------------------------------------------


defaultAccount
=====================

.. code-block:: javascript

    app3.apis.defaultAccount

This default address is used as the default ``"from"`` property, if no ``"from"`` property is specified in for the following methods:

- :ref:`app3.apis.sendTransaction() <apis-sendtransaction>`
- :ref:`app3.apis.call() <apis-call>`
- :ref:`new app3.apis.Contract() -> myContract.methods.myMethod().call() <apis-contract-call>`
- :ref:`new app3.apis.Contract() -> myContract.methods.myMethod().send() <apis-contract-send>`

--------
Property
--------


``String`` - 20 Bytes: Any APIS address. You should have the private key for that address in your node or keystore. (Default is ``undefined``)


-------
Example
-------


.. code-block:: javascript

    app3.apis.defaultAccount;
    > undefined

    // set the default account
    app3.apis.defaultAccount = '0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe';


------------------------------------------------------------------------------

.. _apis-defaultblock:

defaultBlock
=====================

.. code-block:: javascript

    app3.apis.defaultBlock

The default block is used for certain methods. You can override it by passing in the defaultBlock as last parameter.
The default value is "latest".

- :ref:`app3.apis.getBalance() <apis-getbalance>`
- :ref:`app3.apis.getCode() <apis-getcode>`
- :ref:`app3.apis.getTransactionCount() <apis-gettransactioncount>`
- :ref:`app3.apis.getStorageAt() <apis-getstorageat>`
- :ref:`app3.apis.call() <apis-call>`
- :ref:`new app3.apis.Contract() -> myContract.methods.myMethod().call() <apis-contract-call>`

----------
Property
----------


Default block parameters can be one of the following:

- ``Number``: A block number
- ``"genesis"`` - ``String``: The genesis block
- ``"latest"`` - ``String``: The latest block (current head of the blockchain)
- ``"pending"`` - ``String``: The currently mined block (including pending transactions)

Default is ``"latest"``


-------
Example
-------

.. code-block:: javascript

    app3.apis.defaultBlock;
    > "latest"

    // set the default block
    app3.apis.defaultBlock = 231;


------------------------------------------------------------------------------

getProtocolVersion
=====================

.. code-block:: javascript

    app3.apis.getProtocolVersion([callback])

Returns the APIS protocol version of the node.

-------
Returns
-------

``Promise`` returns ``String``: the protocol version.


-------
Example
-------

.. code-block:: javascript

    app3.apis.getProtocolVersion()
    .then(console.log);
    > "63"


------------------------------------------------------------------------------


isSyncing
=====================

.. code-block:: javascript

    app3.apis.isSyncing([callback])

Checks if the node is currently syncing and returns either a syncing object, or ``false``.

.. _apis-issyncing-return:

-------
Returns
-------

``Promise`` returns ``Object|Boolean`` - A sync object when the node is currently syncing or ``false``:

    - ``startingBlock`` - ``Number``: The block number where the sync started.
    - ``currentBlock`` - ``Number``: The block number where at which block the node currently synced to already.
    - ``highestBlock`` - ``Number``: The estimated block number to sync to.


-------
Example
-------

.. code-block:: javascript

    app3.apis.isSyncing()
    .then(console.log);

    > {
        startingBlock: 40606,
        currentBlock: 40612,
        highestBlock: 40612,
    }


------------------------------------------------------------------------------


getCoinbase
=====================

.. code-block:: javascript

    getCoinbase([callback])

Returns the coinbase address to which mining rewards will go.

-------
Returns
-------

``Promise`` returns ``String`` - bytes 20: The coinbase address set in the node for mining rewards.


-------
Example
-------

.. code-block:: javascript

    app3.apis.getCoinbase()
    .then(console.log);
    > "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe"


------------------------------------------------------------------------------

isMining
=====================

.. code-block:: javascript

    app3.apis.isMining([callback])


Checks whapiser the node is mining or not.

-------
Returns
-------

``Promise`` returns ``Boolean``: ``true`` if the node is mining, otherwise ``false``.


-------
Example
-------

.. code-block:: javascript

    app3.apis.isMining()
    .then(console.log);
    > true


------------------------------------------------------------------------------

.. _apis-gasprice:


getGasPrice
=====================

.. code-block:: javascript

    app3.apis.getGasPrice([callback])


Returns the current gas price oracle.
The gas price is determined by the last few blocks median gas price.

-------
Returns
-------

``Promise`` returns ``String`` - Number string of the current gas price in :ref:`wei <what-is-wei>`.

See the :ref:`A note on dealing with big numbers in JavaScript <big-numbers-in-javascript>`.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getGasPrice()
    .then(console.log);
    > "50000000000"


------------------------------------------------------------------------------


getAccounts
=====================

.. code-block:: javascript

    app3.apis.getAccounts([callback])

Returns a list of accounts the node controls.

-------
Returns
-------


``Promise`` returns ``Array`` - Array of accounts controlled by node objects.

The structure of the returned account ``Object`` in the ``Array`` looks as follows:

- ``address`` 32 Bytes - ``String``: address of account
- ``index`` - ``Number``: The index position of accounts controlled by node.
- ``aAPIS`` - ``Number``: The current APIS balance for the given account in atto.
- ``aMNR`` - ``Number``: The current MNR balance for the given account in atto.
- ``nonce`` - ``Number``: The number of transactions
- ``APIS`` 32 Bytes - ``String``: The current APIS balance for the given account.
- ``MNR`` 32 Bytes - ``String``: The current MNR balance for the given account.
- ``proofKey`` 32 Bytes - ``String``: 2-Step Verification Key. ``null`` if not registered.
- ``isMasternode`` = ``Boolean``: True if given address is a masternode.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getAccounts()
    .then(console.log);
    > [ { address: '0x2947e8f4822fef47241d619910050a5c3660c0b9',
          index: 0,
          aAPIS: '1551344056726774550000000',
          aMNR: '20000000000000000',
          nonce: '0',
          APIS: '1_551_344.05672677455',
          MNR: '0.02' },
        { address: '0x036684e72c49c0121823c647587bbd7676d0b998',
          index: 1,
          aAPIS: '0',
          aMNR: '0',
          nonce: '1',
          APIS: '0',
          MNR: '0',
          proofKey: '0x700dc8874a7e187a0e259e6293d839b98ea5c6a9'} ]


------------------------------------------------------------------------------


getWalletInfo
=====================

.. code-block:: javascript

    app3.apis.getWalletInfo(addressOrMask [, callback])

Returns a information of given address.

----------
Parameters
----------

1. ``String`` - The address to get the information. Or the string mask like ``"some_name@some_domain"``

-------
Returns
-------


``Promise`` returns ``Object`` - Information of given address.

- ``address`` 32 Bytes - ``String``: address of account.
- ``mask`` - ``String``: Mask of given address. ``null`` if not registered.
- ``aAPIS`` - ``Number``: The current APIS balance for the given account in atto.
- ``aMNR`` - ``Number``: The current MNR balance for the given account in atto.
- ``nonce`` - ``Number``: The number of transactions
- ``APIS`` 32 Bytes - ``String``: The current APIS balance for the given account.
- ``MNR`` 32 Bytes - ``String``: The current MNR balance for the given account.
- ``proofKey`` 32 Bytes - ``String``: 2-Step Verification Key. ``null`` if not registered.
- ``isContract`` - ``String``: True if given address has contract code. ``null`` if hasn't.
- ``isMasternode`` = ``Boolean``: True if given address is a masternode.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getWalletInfo('0xea31b942f886fcbbcfedd5580f992afe464a38b8');
    // Or app3.apis.getWalletInfo('Daryl@me')
    .then(console.log);
    > { address: '0xea31b942f886fcbbcfedd5580f992afe464a38b8',
        mask: 'Daryl@me',
        aAPIS: '45368491814594000000000',
        aMNR: '627846000000000',
        nonce: '5',
        APIS: '45_368.491814594',
        MNR: '0.000627846',
        isMasternode: false }


------------------------------------------------------------------------------


getBlockNumber
=====================

.. code-block:: javascript

    app3.apis.getBlockNumber([callback])

Returns the current block number.

-------
Returns
-------

``Promise`` returns ``Number`` - The number of the most recent block.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getBlockNumber()
    .then(console.log);
    > 2744


------------------------------------------------------------------------------



getBalance
=====================

.. code-block:: javascript

    app3.apis.getBalance(address [, defaultBlock] [, callback])

Get the balance of an address at a given block.

----------
Parameters
----------

1. ``String`` - The address to get the balance of.
2. ``Number|String`` - (optional) If you pass this parameter it will not use the default block set with :ref:`app3.apis.defaultBlock <apis-defaultblock>`.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------


``Promise`` returns ``Object`` - The current balance for the given address in :ref:`atto <what-is-atto>`.

See the :ref:`A note on dealing with big numbers in JavaScript <big-numbers-in-javascript>`.

- ``aAPIS`` - ``Number``: The current APIS balance for the given account in atto.
- ``aMNR`` - ``Number``: The current MNR balance for the given account in atto.
- ``APIS`` - ``Number``: The current readable APIS balance for the given account.
- ``MNR`` - ``Number``: The current readable MNR balance for the given account.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getBalance("0x407d73d8a49eeb85d32cf465507dd71d507100c1")
    .then(console.log);
    > { attoAPIS: '1553284456726774550000000',
        attoMNR: '20000000000000000',
        APIS: '1_553_284.45672677455',
        MNR: '0.02' }


------------------------------------------------------------------------------

getCode
=====================

.. code-block:: javascript

    app3.apis.getCode(address [, defaultBlock] [, callback])

Get the code at a specific address.

----------
Parameters
----------

1. ``String`` - The address to get the code from.
2. ``Number|String`` - (optional) If you pass this parameter it will not use the default block set with :ref:`app3.apis.defaultBlock <apis-defaultblock>`.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------


``Promise`` returns ``String`` - The data at given address ``address``.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getCode("0xd5677cf67b5aa051bb40496e68ad359eb97cfbf8")
    .then(console.log);
    > "0x600160008035811a818181146012578301005b601b6001356025565b8060005260206000f25b600060078202905091905056"


------------------------------------------------------------------------------

.. _apis-getblock:

getBlock
=====================

.. code-block:: javascript

     app3.apis.getBlock(blockHashOrBlockNumber [, returnTransactionObjects] [, callback])

Returns a block matching the block number or block hash.

----------
Parameters
----------

1. ``String|Number`` - The block number or block hash. Or the string ``"genesis"``, ``"latest"`` or ``"pending"`` as in the :ref:`default block parameter <apis-defaultblock>`.
2. ``Boolean`` - (optional, default ``false``) If ``true``, the returned block will contain all transactions as objects, if ``false`` it will only contains the transaction hashes.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------


``Promise`` returns ``Object`` - The block object:

  - ``number`` - ``Number``: The block number. ``null`` when its pending block.
  - ``hash`` 32 Bytes - ``String``: Hash of the block. ``null`` when its pending block.
  - ``parentHash`` 32 Bytes - ``String``: Hash of the parent block.
  - ``nonce`` - ``Number``: Balance of miner (10 blocks ago).
  - ``txTrieHash`` 32 Bytes - ``String``: The root of the transaction trie of the block
  - ``stateRoot`` 32 Bytes - ``String``: The root of the final state trie of the block.
  - ``coinbase`` - ``String``: The address of the beneficiary to whom the mining rewards were given.
  - ``coinbaseMask`` - ``String``: The mask of the coinbase.
  - ``rewardPoint`` - ``String``: Integer of the RP for this block of miner.
  - ``cumulativeRewardPoint`` - ``String``: Integer of the cumulative RP of the chain until this block.
  - ``extraData`` - ``String``: The "extra data" field of this block.
  - ``gasLimit`` - ``Number``: The maximum gas allowed in this block.
  - ``gasUsed`` - ``Number``: The total used gas by all transactions in this block.
  - ``mineralUsed`` - ``Number``: The total used mineral by all transactions in this block.
  - ``timestamp`` - ``Number``: The unix timestamp for when the block was collated.
  - ``transactions`` - ``Array``: Array of transaction objects, or 32 Bytes transaction hashes depending on the ``returnTransactionObjects`` parameter.
  - ``logsBloom`` 256 Bytes - ``String``: The bloom filter for the logs of the block. ``null`` when its pending block.
  - ``mnHash`` 32 Bytes - ``String``: Hash of the masternodes
  - ``mnReward`` - ``Number``: Base amount of Masternode rewards
  - ``mnGenerals`` - ``Array``: Array of general masternodes.
  - ``mnMajors`` - ``Array``: Array of major masternodes.
  - ``mnPrivates`` - ``Array``: Array of private masternodes.
  - ``size`` - ``Number``: Integer the size of this block in bytes.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getBlock(3150)
    .then(console.log);

    > { number: 3000,
        hash: '0xb3b51d689be882447e2a9a94be94c67603a921f9394af013a2ce0b4657f3f93d',
        parentHash: '0x62c51711e287f3330e8efe638c7b166788f732ad758eaebc4707720125b02ff6',
        coinbase: '0x643d122906cdaa4468702696431da16dec7d5ad2',
        stateRoot: '0xbca98d3790068316689df10bbc0851d32df79c23400fa93a36121ff04a44b1a4',
        txTrieHash: '0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421',
        receiptsTrieHash: '0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421',
        rewardPoint: '682438316916457871243767603736587500000000',
        cumulativeRewardPoint: '6518389044590527146719249904157700506472824276408000000',
        gasLimit: 50000000,
        gasUsed: 0,
        mineralUsed: '0',
        timestamp: 1541696090,
        extraData: '0x4150495320706f776572656420536572766572',
        rpSeed: '0x356e54d588078c71cf1737ec360accc1c5e8a4a94bf77eb4429421587b5f0fa8',
        nonce: '0x3bcbc228e38b6ce5f100',
        transactions: [],
        logsBloom: '0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
        mnHash: '',
        mnReward: '0.000000000000000000',
        mnGenerals: [],
        mnMajors: [],
        mnPrivates: [],
        size: 613 }


------------------------------------------------------------------------------


getBlockTransactionCount
=====================

.. code-block:: javascript

    app3.apis.getBlockTransactionCount(blockHashOrBlockNumber [, callback])

Returns the number of transaction in a given block.

----------
Parameters
----------


1. ``String|Number`` - The block number or hash. Or the string ``"genesis"``, ``"latest"`` or ``"pending"`` as in the :ref:`default block parameter <apis-defaultblock>`.
2. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------


``Promise`` returns ``Number`` - The number of transactions in the given block.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getBlockTransactionCount('0xb3b51d689be882447e2a9a94be94c67603a921f9394af013a2ce0b4657f3f93d')
    .then(console.log);
    > 0


------------------------------------------------------------------------------


getTransaction
=====================

.. code-block:: javascript

    app3.apis.getTransaction(transactionHash [, callback])

Returns a transaction matching the given transaction hash.

----------
Parameters
----------

1. ``String`` - The transaction hash.
2. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


.. _apis-gettransaction-return:

-------
Returns
-------


``Promise`` returns ``Object`` - A transaction object its hash ``transactionHash``:

  - ``hash`` 32 Bytes - ``String``: Hash of the transaction.
  - ``nonce`` - ``Number``: The number of transactions made by the sender prior to this one.
  - ``blockHash`` 32 Bytes - ``String``: Hash of the block where this transaction was in. ``null`` when its pending.
  - ``blockNumber`` - ``Number``: Block number where this transaction was in. ``null`` when its pending.
  - ``transactionIndex`` - ``Number``: Integer of the transactions index position in the block. ``null`` when its pending.
  - ``from`` - ``String``: Address of the sender.
  - ``to`` - ``String``: Address of the receiver. ``null`` when its a contract creation transaction.
  - ``toMask`` - ``String``: Mask of the receiver address. ``null`` when its a contract creation transaction or not registered.
  - ``value`` - ``String``: Value transferred in :ref:`wei <what-is-wei>`.
  - ``valueAPIS`` - ``String``: Value readable.
  - ``gasPrice`` - ``String``: Gas price provided by the sender in :ref:`wei <what-is-wei>`.
  - ``gasPriceAPIS`` - ``String``: Gas price readable.
  - ``gas`` - ``Number``: Gas provided by the sender.
  - ``feeLimitAPIS`` - ``String``: The maximum chargeable amount (gasLimit x gasPrice).
  - ``data`` - ``String``: The data sent along with the transaction.
  - ``r`` - ``String``: 'r' value of signature. ``null` when its not signed.
  - ``s`` - ``String``: 's' value of signature. ``null` when its not signed.
  - ``v`` - ``String``: 'v' value of signature. ``null` when its not signed.
  - ``certR`` - ``String``: 'r' value of authentication. ``null`` when its not authorized by knowledge key.
  - ``certS`` - ``String``: 's' value of authentication. ``null`` when its not authorized by knowledge key.
  - ``certV`` - ``String``: 'v' value of authentication. ``null`` when its not authorized by knowledge key.


-------
Example
-------


.. code-block:: javascript

    app3.apis.getTransaction('0x9fc76417374aa880d4449a1f7f31ec597f00b1f6f3dd2d66f4c9c6c445836d8b§234')
    .then(console.log);

    > { hash: '0x960d6a7bfa4db0b32b59c473af05d9ae6975d0b45f29d38b83564d26cc9342dc',
        nonce: 4,
        blockHash: '0x9dd1ec4ca42d54805e8dbdbd37909a9dfd91a783d18dd7cbb9bbe3805ec777b3',
        blockNumber: 40995,
        transactionIndex: 0,
        from: '0xea31B942F886fcBBcFeDd5580F992Afe464A38B8',
        to: '0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe',
        value: '10000000000000000000000',
        valueAPIS: '10_000',
        gas: 200000,
        gasPrice: '50000000000',
        gasPriceAPIS: '0.00000005',
        feeLimitAPIS: '0.01',
        data: '',
        r: '0x4b01bc76bc2682d04e9a7cdcb3526d56da54e63d1a7087f0607f3e0f72744e66',
        s: '0x6628fda2e57ebc99a3e59acef5e95f7063d9ff06d1fa76d33a84c5032d576d44',
        v: '0x1c' }

------------------------------------------------------------------------------


getTransactionsByKeyword
========================

.. code-block:: javascript

    app3.apis.getTransactionsByKeyword(keyword, rowCount, offset)

Returns a transaction list matching the given keyword.

----------
Parameters
----------

1. ``String`` - Keywords for retrieving transactions. ie. transaction hash, address, address mask.
2. ``Number`` - Maximum number of search results.
3. ``Number`` - Number of skipped search results.


.. _apis-gettransactionbykeyword-return:

-------
Returns
-------


``Promise`` returns ``Object`` - A transaction object its hash ``transactionHash``:

  - ``status`` - ``Boolean``: ``TRUE`` if the transaction was successful, ``FALSE``, if the EVM reverted the transaction.
  - ``blockHash`` 32 Bytes - ``String``: Hash of the block where this transaction was in.
  - ``blockNumber`` - ``Number``: Block number where this transaction was in.
  - ``timestamp`` - ``Number``: The unix timestamp for when the block was collated.
  - ``transactionHash`` 32 Bytes - ``String``: Hash of the transaction.
  - ``from`` - ``String``: Address of the sender.
  - ``to`` - ``String``: Address of the receiver. ``null`` when its a contract creation transaction.
  - ``toMask`` - ``String``: Mask of the receiver. ``null`` when its not registered.
  - ``contractAddress`` - ``String``: The contract address created, if the transaction was a contract creation, otherwise ``null``.
  - ``gas`` - ``Number``: Gas provided by the sender.
  - ``gasPrice`` - ``String``: Gas price provided by the sender in atto.
  - ``gasPriceAPIS`` - ``String``: Gas price in APIS.
  - ``gasUsed``- ``Number``:  The amount of gas used by this specific transaction alone.
  - ``mineralUsed`` - ``String``: The amount of mineral used by this specific transaction alone.
  - ``mineralUsedMNR`` - ``String``: Used mineral in MNR.
  - ``feePaid`` - ``String``: Finally paid fee amount in atto.
  - ``feePaidAPIS`` - ``String``: Paid fee amount in APIS.


-------
Example
-------


.. code-block:: javascript

    app3.apis.getTransactionsByKeyword('apis@me', 10, 0)
    .then(console.log);

    > [ { status: '0x01',
    transactionHash: '0x7a14dbb3b0bf4d2fcb03c4ddf9dbb503ee1a4342c39bf702484a48c8d6898335',
    blockHash: '0xc53da41b4e58211307709654a96caff537fb99d6f2827487da76d96b6886825e',
    blockNumber: 323,
    timestamp: '1545202180',
    from: '0x891122cb40b2a83b2686107720d84c7eb7f37ad4',
    to: '0x1000000000000000000000000000000000037453',
    gas: 266581,
    gasPrice: '50000000000',
    gasPriceAPIS: '0.00000005',
    gasUsed: 265971,
    fee: '13298550000000000',
    feeAPIS: '0.01329855',
    mineralUsed: '13298550000000000',
    mineralUsedMNR: '0.01329855',
    feePaid: '0',
    feePaidAPIS: '0' },
  { status: '0x01',
    transactionHash: '0xaffe6524f8af6484d23bdd362709e3c26373999175a72a356cc6174636814293',
    blockHash: '0xf591aef448e402998b87fdc6237be2e02a3b7721118a40468afcea216de6924a',
    blockNumber: 318,
    timestamp: '1545202140',
    from: '0x891122cb40b2a83b2686107720d84c7eb7f37ad4',
    to: '0x1000000000000000000000000000000000037453',
    gas: 266581,
    gasPrice: '50000000000',
    gasPriceAPIS: '0.00000005',
    gasUsed: 265971,
    fee: '13298550000000000',
    feeAPIS: '0.01329855',
    mineralUsed: '13298550000000000',
    mineralUsedMNR: '0.01329855',
    feePaid: '0',
    feePaidAPIS: '0' } ]

------------------------------------------------------------------------------


getRecentTransactions
=====================

.. code-block:: javascript

    app3.apis.getRecentTransactions([rowCount] [, offset])

Returns a recent transaction list matching the given condition.

----------
Parameters
----------

1. ``Number`` - Maximum number of search results. Default is 20
2. ``Number`` - Number of skipped search results. Default is 0


.. _apis-getrecenttransactions-return:

-------
Returns
-------


``Promise`` returns ``Object`` - A list of transaction object:

  - ``status`` - ``Boolean``: ``TRUE`` if the transaction was successful, ``FALSE``, if the EVM reverted the transaction.
  - ``blockHash`` 32 Bytes - ``String``: Hash of the block where this transaction was in.
  - ``blockNumber`` - ``Number``: Block number where this transaction was in.
  - ``timestamp`` - ``Number``: The unix timestamp for when the block was collated.
  - ``transactionHash`` 32 Bytes - ``String``: Hash of the transaction.
  - ``from`` - ``String``: Address of the sender.
  - ``to`` - ``String``: Address of the receiver. ``null`` when its a contract creation transaction.
  - ``toMask`` - ``String``: Mask of the receiver. ``null`` when its not registered.
  - ``contractAddress`` - ``String``: The contract address created, if the transaction was a contract creation, otherwise ``null``.
  - ``gas`` - ``Number``: Gas provided by the sender.
  - ``gasPrice`` - ``String``: Gas price provided by the sender in atto.
  - ``gasPriceAPIS`` - ``String``: Gas price in APIS.
  - ``gasUsed``- ``Number``:  The amount of gas used by this specific transaction alone.
  - ``mineralUsed`` - ``String``: The amount of mineral used by this specific transaction alone.
  - ``mineralUsedMNR`` - ``String``: Used mineral in MNR.
  - ``feePaid`` - ``String``: Finally paid fee amount in atto.
  - ``feePaidAPIS`` - ``String``: Paid fee amount in APIS.


-------
Example
-------


.. code-block:: javascript

    app3.apis.getRecentTransactions(0, 0)
    .then(console.log);

    > [ { status: '0x01',
    transactionHash: '0x0172b3308acd1149b2100f20434752ba3978a64ae8c26231b5e994f7e18cba2e',
    blockHash: '0xab3bae560a30289894bdb809e342ee4292df0e2ecdc379d852f06f0e3994f4e4',
    blockNumber: 1049,
    timestamp: '1545293545',
    from: '0x89112261d222abc94d257acb4ad470ca911fcfb6',
    to: '0x89112261d222abc94d257acb4ad470ca911fcfb6',
    gas: 220000,
    gasPrice: '77777777777',
    gasPriceAPIZ: '0.000000077777777777',
    gasUsed: 213000,
    fee: '16566666666501000',
    feeAPIZ: '0.016566666666501',
    mineralUsed: '16566666666501000',
    mineralUsedMNR: '0.016566666666501',
    feePaid: '0',
    feePaidAPIZ: '0' },
  { status: '0x01',
    transactionHash: '0x744b4e976c63a68f168044b4268be011918ae43a28b77b37515af4443a2d1fbd',
    blockHash: '0x59798b2ee3c7c6dab018c4379337927f6f0838dcf85ca365f0d78cbaf6bdb832',
    blockNumber: 1047,
    timestamp: '1545293529',
    from: '0x89112261d222abc94d257acb4ad470ca911fcfb6',
    to: '0x891122cb40b2a83b2686107720d84c7eb7f37ad4',
    gas: 200000,
    gasPrice: '50000000000',
    gasPriceAPIZ: '0.00000005',
    gasUsed: 200000,
    fee: '10000000000000000',
    feeAPIZ: '0.01',
    mineralUsed: '10000000000000000',
    mineralUsedMNR: '0.01',
    feePaid: '0',
    feePaidAPIZ: '0' }, ... ]

------------------------------------------------------------------------------

.. _apis-getrecentblocks:

getRecentBlocks
=====================

.. code-block:: javascript

     app3.apis.getRecentBlock([rowCount] [, offset])

Returns a block list of recent confirmed.

----------
Parameters
----------

1. ``Number`` - Maximum number of search results. Default is 20
2. ``Number`` - Number of skipped search results. Default is 1

-------
Returns
-------


``Promise`` returns ``Array`` - The list of block object:

  - ``number`` - ``Number``: The block number. ``null`` when its pending block.
  - ``hash`` 32 Bytes - ``String``: Hash of the block. ``null`` when its pending block.
  - ``parentHash`` 32 Bytes - ``String``: Hash of the parent block.
  - ``nonce`` - ``Number``: Balance of miner (10 blocks ago).
  - ``txTrieHash`` 32 Bytes - ``String``: The root of the transaction trie of the block
  - ``stateRoot`` 32 Bytes - ``String``: The root of the final state trie of the block.
  - ``coinbase`` - ``String``: The address of the beneficiary to whom the mining rewards were given.
  - ``coinbaseMask`` - ``String``: The mask of the coinbase.
  - ``rewardPoint`` - ``String``: Integer of the RP for this block of miner.
  - ``cumulativeRewardPoint`` - ``String``: Integer of the cumulative RP of the chain until this block.
  - ``extraData`` - ``String``: The "extra data" field of this block.
  - ``gasLimit`` - ``Number``: The maximum gas allowed in this block.
  - ``gasUsed`` - ``Number``: The total used gas by all transactions in this block.
  - ``mineralUsed`` - ``Number``: The total used mineral by all transactions in this block.
  - ``timestamp`` - ``Number``: The unix timestamp for when the block was collated.
  - ``transactions`` - ``Array``: Array of transaction objects, or 32 Bytes transaction hashes depending on the ``returnTransactionObjects`` parameter.
  - ``logsBloom`` 256 Bytes - ``String``: The bloom filter for the logs of the block. ``null`` when its pending block.
  - ``mnHash`` 32 Bytes - ``String``: Hash of the masternodes
  - ``mnReward`` - ``Number``: Base amount of Masternode rewards
  - ``mnGenerals`` - ``Array``: Array of general masternodes.
  - ``mnMajors`` - ``Array``: Array of major masternodes.
  - ``mnPrivates`` - ``Array``: Array of private masternodes.
  - ``size`` - ``Number``: Integer the size of this block in bytes.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getRecentBlocks()
    .then(console.log);

    > [ { number: 1196,
    hash: '0x5c6eb8638c7636bd033636e0cea62d667ccc4224a552f93390e12b021c68e822',
    parentHash: '0x2280ee46a61af1557d1562f7c241ec31a51694c713772a1176d51565d655e820',
    coinbase: '0xc779d025076d31d26d4e73af90df1f298ebe5b0a',
    stateRoot: '0xa72bc8f9d5a0fd1696a43d4745ca1146cd4a28252c85af3d48d99c1546a33392',
    txTrieHash: '0x83633cadfa85ff78c477e90c9b804364a9c9bdb0d4c362e189ef3464819edafb',
    receiptsTrieHash: '0x771dbd196da19ef46e74b7d7e57444bfbcf48ebbd3eae20903849d9219a1fcaa',
    rewardPoint: '1018322121669577712222032940000000000000000000',
    cumulativeRewardPoint: '4270753430417833658920855884570051589608925547500000000',
    gasLimit: 200000000,
    gasUsed: 200000,
    mineralUsed: '1800000000000000',
    timestamp: '1545294721',
    extraData: '0x4150495320706f776572656420536572766572',
    rpSeed: '0xfac3cce593d5c93c514baf22c6fea42f002bb1ec2eb24f593d7553174111202a',
    nonce: '0x01e6b2c7688471080000',
    txSize: 1,
    transactions:
     [ '0xed514ce278bf40160a71b85c2023c850a493fdb6880cad1118434d1c057f99af' ],
    logsBloom: '0x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
    size: 743 },
  { number: 1197,
    hash: '0x0804718c1f632054efc8fc00e88ffaa978b07e2195ea333923f9edca1fdabd60',
    parentHash: '0x5c6eb8638c7636bd033636e0cea62d667ccc4224a552f93390e12b021c68e822',
    coinbase: '0x643d122906cdaa4468702696431da16dec7d5ad2',
    stateRoot: '0xeb23f13319b1ffdbd8df80054311afc66c50a72ac7f9c8fcf1ac0577028a9192',
    txTrieHash: '0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421',
    receiptsTrieHash: '0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421',
    rewardPoint: '1794744506802002057974148250592699965000000000000',
    cumulativeRewardPoint: '4270755225162340460922913858718302182308890547500000000',
    gasLimit: 200000000,
    gasUsed: 0,
    mineralUsed: '0',
    timestamp: '1545294729',
    extraData: '0x4150495320706f776572656420536572766572',
    rpSeed: '0xb64c17597776982dd4e2bbc493aa660ae0b6cc2dd8203252657b789d7816e419',
    nonce: '0x2e178224f7d0f953a800',
    txSize: 0,
    transactions: [],
    size: 616 }, ... ]


------------------------------------------------------------------------------

getTransactionFromBlock
=====================

.. code-block:: javascript

    getTransactionFromBlock(hashStringOrNumber, indexNumber [, callback])

Returns a transaction based on a block hash or number and the transactions index position.

----------
Parameters
----------


1. ``String`` - A block number or hash. Or the string ``"genesis"``, ``"latest"`` or ``"pending"`` as in the :ref:`default block parameter <apis-defaultblock>`.
2. ``Number`` - The transactions index position.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------


``Promise`` returns ``Object`` - A transaction object, see :ref:`app3.apis.getTransaction <apis-gettransaction-return>`:


-------
Example
-------


.. code-block:: javascript

    var transaction = app3.apis.getTransactionFromBlock('0x4534534534', 2)
    .then(console.log);
    > // see app3.apis.getTransaction



------------------------------------------------------------------------------


getTransactionReceipt
=====================

.. code-block:: javascript

    app3.apis.getTransactionReceipt(hash [, callback])

Returns the receipt of a transaction by transaction hash.

.. note:: The receipt is not available for pending transactions and returns ``null``.


----------
Parameters
----------

1. ``String`` - The transaction hash.
2. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


.. _apis-gettransactionreceipt-return:

-------
Returns
-------


``Promise`` returns ``Object`` - A transaction receipt object, or ``null`` when no receipt was found:

  - ``status`` - ``Boolean``: ``TRUE`` if the transaction was successful, ``FALSE``, if the EVM reverted the transaction.
  - ``blockHash`` 32 Bytes - ``String``: Hash of the block where this transaction was in.
  - ``blockNumber`` - ``Number``: Block number where this transaction was in.
  - ``transactionHash`` 32 Bytes - ``String``: Hash of the transaction.
  - ``transactionIndex``- ``Number``: Integer of the transactions index position in the block.
  - ``timestamp``- ``Number``: The unix timestamp for when the block was collated.
  - ``from`` - ``String``: Address of the sender.
  - ``to`` - ``String``: Address of the receiver. ``null`` when its a contract creation transaction.
  - ``toMask`` - ``String``: Mask of the receiver. ``null`` when its not registered.
  - ``contractAddress`` - ``String``: The contract address created, if the transaction was a contract creation, otherwise ``null``.
  - ``gas`` - ``Number``: Gas provided by the sender.
  - ``gasPrice`` - ``String``: Gas price provided by the sender in atto.
  - ``gasPriceAPIS`` - ``String``: Gas price in APIS.
  - ``gasUsed``- ``Number``:  The amount of gas used by this specific transaction alone.
  - ``mineralUsed`` - ``String``: The amount of mineral used by this specific transaction alone.
  - ``mineralUsedMNR`` - ``String``: Used mineral in MNR.
  - ``feePaid`` - ``String``: Finally paid fee amount in atto.
  - ``feePaidAPIS`` - ``String``: Paid fee amount in APIS.
  - ``cumulativeGasUsed`` - ``Number``: The total amount of gas used when this transaction was executed in the block.
  - ``cumulativeMineralUsed`` - ``Number``: The total amount of mineral used when this transaction was executed in the block.
  - ``cumulativeMineralUsedMNR`` - ``Number``: Cumulative mineral in MNR
  - ``logs`` - ``Array``: Array of log objects, which this transaction generated.
  - ``internalTransaction`` - ``Array``: Array of internal transaction objects.

-------
Example
-------

.. code-block:: javascript

    var receipt = app3.apis.getTransactionReceipt('0x9fc76417374aa880d4449a1f7f31ec597f00b1f6f3dd2d66f4c9c6c445836d8b')
    .then(console.log);

    > { status: true,
        transactionHash: '0x9fc76417374aa880d4449a1f7f31ec597f00b1f6f3dd2d66f4c9c6c445836d8b',
        transactionIndex: 0,
        blockHash: '0x9dd1ec4ca42d54805e8dbdbd37909a9dfd91a783d18dd7cbb9bbe3805ec777b3',
        blockNumber: 40995,
        from: '0xea31b942f886fcbbcfedd5580f992afe464a38b8',
        to: '0xea31b942f886fcbbcfedd5580f992afe464a38b8',
        gas: 200000,
        gasPrice: '50000000000',
        gasPriceAPIS: '0.00000005',
        gasUsed: 200000,
        fee: '10000000000000000',
        feeAPIS: '0.01',
        mineralUsed: '10503000000000',
        mineralUsedMNR: '0.000010503',
        feePaid: '9989497000000000',
        feePaidAPIS: '0.009989497',
        cumulativeGasUsed: 200000,
        cumulativeMineralUsed: '10503000000000',
        cumulativeMineralUsedMNR: '0.000010503'
        logs: [{
             // logs as returned by getPastLogs, etc.
        }, ...] }

------------------------------------------------------------------------------


.. _apis-gettransactioncount:

getTransactionCount
=====================

.. code-block:: javascript

    app3.apis.getTransactionCount(address [, defaultBlock] [, callback])

Get the numbers of transactions sent from this address.

----------
Parameters
----------

1. ``String`` - The address to get the numbers of transactions from.
2. ``Number|String`` - (optional) If you pass this parameter it will not use the default block set with :ref:`app3.apis.defaultBlock <apis-defaultblock>`.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------


``Promise`` returns ``Number`` - The number of transactions sent from the given address.

-------
Example
-------


.. code-block:: javascript

    app3.apis.getTransactionCount("0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe")
    .then(console.log);
    > 1


------------------------------------------------------------------------------


.. _apis-getmasternodecount:

getMasternodeCount
==================

.. code-block:: javascript

    app3.apis.getMasternodeCount([, callback])

Get the number of masternodes.

Masternode State :
  - Earlybird - Masternode that can be joined through apis.mn. The nodes are joined to the day(10,800 blocks) before the round begins.
  - Normal - Masternode joined through APIS Core within the first day(10,800 blocks) of the round
  - Late - Masternode joined through the APIS Core after the normal participation period of the round

----------
Parameters
----------

1. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------


``Promise`` returns ``Object`` - The number of masternodes.

  - ``generalEarly`` - ``Number``: Number of General Earlybird Masternodes
  - ``majorEarly`` - ``Number``: Number of Major Earlybird Masternodes
  - ``privateEarly`` - ``Number``: Number of Private Earlybird Masternodes
  - ``generalNormal`` - ``Number``: Number of General Normal Masternodes
  - ``majorNormal`` - ``Number``: Number of Major Normal Masternodes
  - ``privateNormal`` - ``Number``: Number of Private Normal Masternodes
  - ``generalLate`` - ``Number``: Number of General Late Masternodes
  - ``majorLate`` - ``Number``: Number of Major Late Masternodes
  - ``privateLate`` - ``Number``: Number of Private Late Masternodes

-------
Example
-------


.. code-block:: javascript

    app3.apis.getMasternodeCount()
    .then(console.log);
    > {
        generalEarly: 3929,
        majorEarly: 2661,
        privateEarly: 1776,
        generalNormal: 0,
        majorNormal: 0,
        privateNormal: 0,
        generalLate: 2,
        majorLate: 1,
        privateLate: 3 }


------------------------------------------------------------------------------

.. _apis-sendtransaction:

sendTransaction
=====================

.. code-block:: javascript

    app3.apis.sendTransaction(transactionObject [, callback])

Sends a transaction to the network.

----------
Parameters
----------


1. ``Object`` - The transaction object to send:
  - ``from`` - ``String|Number``: The address for the sending account. Uses the :ref:`app3.apis.defaultAccount <apis-defaultaccount>` property, if not specified. Or an address or index of a local wallet in :ref:`app3.apis.accounts.wallet <apis_accounts_wallet>`.
  - ``to`` - ``String``: (optional) The destination address of the message, left undefined for a contract-creation transaction.
  - ``value`` - ``Number|String|BN|BigNumber``: (optional) The value transferred for the transaction in :ref:`atto <what-is-atto>`, also the endowment if it's a contract-creation transaction.
  - ``gas``  - ``Number``: (optional, default: To-Be-Determined) The amount of gas to use for the transaction (unused gas is refunded).
  - ``gasPrice`` - ``Number|String|BN|BigNumber``: (optional) The price of gas for this transaction in :ref:`atto <what-is-atto>`, defaults to :ref:`app3.apis.gasPrice <apis-gasprice>`.
  - ``data`` - ``String``: (optional) Either a `ABI byte string <http://solidity.readthedocs.io/en/latest/abi-spec.html>`_ containing the data of the function call on a contract, or in the case of a contract-creation transaction the initialisation code.
  - ``nonce`` - ``Number``: (optional) Integer of a nonce. This allows to overwrite your own pending transactions that use the same nonce.
2. ``callback`` - ``Function``: (optional) Optional callback, returns an error object as first parameter and the result as second.

.. note:: The ``from`` property can also be an address or index from the :ref:`app3.apis.accounts.wallet <apis_accounts_wallet>`. It will then sign locally using the private key of that account, and send the transaction via :ref:`app3.apis.sendSignedTransaction() <apis-sendsignedtransaction>`.

.. _apis-sendtransaction-return:

-------
Returns
-------

The **callback** will return the 32 bytes transaction hash.

``PromiEvent``: A :ref:`promise combined event emitter <promiEvent>`. Will be resolved when the transaction :ref:`receipt <apis-gettransactionreceipt-return>` is available. Additionally the following events are available:

- ``"transactionHash"`` returns ``String``: Is fired right after the transaction is sent and a transaction hash is available.
- ``"receipt"`` returns ``Object``: Is fired when the transaction receipt is available.
- ``"confirmation"`` returns ``Number``, ``Object``: Is fired for every confirmation up to the 12th confirmation. Receives the confirmation number as the first and the :ref:`receipt <apis-gettransactionreceipt-return>` as the second argument. Fired from confirmation 0 on, which is the block where its minded.
- ``"error"`` returns ``Error``: Is fired if an error occurs during sending. If a out of gas error, the second parameter is the receipt.


-------
Example
-------

.. code-block:: javascript

    // compiled solidity source code using https://remix.ethereum.org
    var code = "603d80600c6000396000f3007c01000000000000000000000000000000000000000000000000000000006000350463c6888fa18114602d57005b6007600435028060005260206000f3";


    // using the callback
    app3.apis.sendTransaction({
        from: '0xde0B295669a9FD93d5F28D9Ec85E40f4cb697BAe',
        data: code // deploying a contracrt
    }, function(error, hash){
        ...
    });

    // using the promise
    app3.apis.sendTransaction({
        from: '0xde0B295669a9FD93d5F28D9Ec85E40f4cb697BAe',
        to: '0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe',
        value: '1000000000000000'
    })
    .then(function(receipt){
        ...
    });


    // using the event emitter
    app3.apis.sendTransaction({
        from: '0xde0B295669a9FD93d5F28D9Ec85E40f4cb697BAe',
        to: '0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe',
        value: '1000000000000000'
    })
    .on('transactionHash', function(hash){
        ...
    })
    .on('receipt', function(receipt){
        ...
    })
    .on('confirmation', function(confirmationNumber, receipt){ ... })
    .on('error', console.error); // If a out of gas error, the second parameter is the receipt.


------------------------------------------------------------------------------

.. _apis-sendsignedtransaction:

sendSignedTransaction
=====================

.. code-block:: javascript

    app3.apis.sendSignedTransaction(signedTransactionData [, callback])

Sends an already signed transaction, generated for example using :ref:`app3.apis.accounts.signTransaction <apis-accounts-signtransaction>`.

----------
Parameters
----------

1. ``String`` - Signed transaction data in HEX format
2. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------


``PromiEvent``: A :ref:`promise combined event emitter <promiEvent>`. Will be resolved when the transaction :ref:`receipt <apis-gettransactionreceipt-return>` is available.

Please see the return values for :ref:`app3.apis.sendTransaction <apis-sendtransaction-return>` for details.

-------
Example
-------


.. code-block:: javascript

    const rawTx = '0xf889808609184e72a00082271094000000000000000000000000000000000000000080a47f74657374320000000000000000000000000000000000000000000000000000006000571ca08a8bbf888cfa37bbf0bb965423625641fc956967b81d12e23709cead01446075a01ce999b56a8a88504be365442ea61239198e23d1fce7d00fcfc5cd3b44b7215f';

    app3.apis.sendSignedTransaction(rawTx)
    .on('receipt', console.log);

    > // see apis.getTransactionReceipt() for details


------------------------------------------------------------------------------


sign
=====================

.. code-block:: javascript

    app3.apis.sign(dataToSign, address [, callback])

Signs data using a specific account. This account needs to be unlocked.

----------
Parameters
----------


1. ``String`` - Data to sign. If String it will be converted using :ref:`app3.utils.utf8ToHex <utils-utf8tohex>`.
2. ``String|Number`` - Address to sign data with. Or an address or index of a local wallet in :ref:`app3.apis.accounts.wallet <apis_accounts_wallet>`.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

.. note:: The 2. ``address`` parameter can also be an address or index from the :ref:`app3.apis.accounts.wallet <apis_accounts_wallet>`. It will then sign locally using the private key of this account.


-------
Returns
-------


``Promise`` returns ``String`` - The signature.


-------
Example
-------


.. code-block:: javascript

    app3.apis.sign("Hello world", "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe")
    .then(console.log);
    > "0x30755ed65396facf86c53e6217c52b4daebe72aa4941d89635409de4c9c7f9466d4e9aaec7977f05e923889b33c0d0dd27d7226b6e6f56ce737465c5cfd04be400"

    // the below is the same
    app3.apis.sign(app3.utils.utf8ToHex("Hello world"), "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe")
    .then(console.log);
    > "0x30755ed65396facf86c53e6217c52b4daebe72aa4941d89635409de4c9c7f9466d4e9aaec7977f05e923889b33c0d0dd27d7226b6e6f56ce737465c5cfd04be400"


------------------------------------------------------------------------------


call
=====================

.. code-block:: javascript

    app3.apis.call(callObject [, defaultBlock] [, callback])

Executes a message call transaction, which is directly executed in the VM of the node, but never mined into the blockchain.

----------
Parameters
----------

1. ``Object`` - A transaction object see :ref:`app3.apis.sendTransaction <apis-sendtransaction-return>`, with the difference that for calls the ``from`` property is optional as well.
2. ``Number|String`` - (optional) If you pass this parameter it will not use the default block set with :ref:`app3.apis.defaultBlock <apis-defaultblock>`.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.

-------
Returns
-------

``Promise`` returns ``String``: The returned data of the call, e.g. a smart contract functions return value.

-------
Example
-------


.. code-block:: javascript

    app3.apis.call({
        to: "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe", // contract address
        data: "0xc6888fa10000000000000000000000000000000000000000000000000000000000000003"
    })
    .then(console.log);
    > "0x000000000000000000000000000000000000000000000000000000000000000a"


------------------------------------------------------------------------------


estimateGas
=====================

.. code-block:: javascript

    app3.apis.estimateGas(callObject [, callback])

Executes a message call or transaction and returns the amount of the gas used.

----------
Parameters
----------


1. ``Object`` - A transaction object see :ref:`app3.apis.sendTransaction <apis-sendtransaction-return>`, with the difference that for calls the ``from`` property is optional as well.
2. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second.


-------
Returns
-------

``Promise`` returns ``Number`` - the used gas for the simulated call/transaction.

-------
Example
-------


.. code-block:: javascript

    app3.apis.estimateGas({
        to: "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe",
        data: "0xc6888fa10000000000000000000000000000000000000000000000000000000000000003"
    })
    .then(console.log);
    > "0x0000000000000000000000000000000000000000000000000000000000000015"


------------------------------------------------------------------------------


getPastLogs
=====================

.. code-block:: javascript

    app3.apis.getPastLogs(options [, callback])

Gets past logs, matching the given options.

----------
Parameters
----------

1. ``Object`` - The filter options as follows:
  - ``fromBlock`` - ``Number|String``: The number of the earliest block (``"latest"`` may be given to mean the most recent and ``"pending"`` currently mining, block). By default ``"latest"``.
  - ``toBlock`` -  ``Number|String``: The number of the latest block (``"latest"`` may be given to mean the most recent and ``"pending"`` currently mining, block). By default ``"latest"``.
  - ``address`` -  ``String|Array``: An address or a list of addresses to only get logs from particular account(s).
  - ``topics`` - ``Array``: An array of values which must each appear in the log entries. The order is important, if you want to leave topics out use ``null``, e.g. ``[null, '0x12...']``. You can also pass an array for each topic with options for that topic e.g. ``[null, ['option1', 'option2']]``


.. _apis-getpastlogs-return:

-------
Returns
-------

``Promise`` returns ``Array`` - Array of log objects.

The structure of the returned event ``Object`` in the ``Array`` looks as follows:

- ``address`` - ``String``: From which this event originated from.
- ``data`` - ``String``: The data containing non-indexed log parameter.
- ``topics`` - ``Array``: An array with max 4 32 Byte topics, topic 1-3 contains indexed parameters of the log.
- ``logIndex`` - ``Number``: Integer of the event index position in the block.
- ``transactionIndex`` - ``Number``: Integer of the transaction's index position, the event was created in.
- ``transactionHash`` 32 Bytes - ``String``: Hash of the transaction this event was created in.
- ``blockHash`` 32 Bytes - ``String``: Hash of the block where this event was created in. ``null`` when its still pending.
- ``blockNumber`` - ``Number``: The block number where this log was created in. ``null`` when still pending.

-------
Example
-------

.. code-block:: javascript

    app3.apis.getPastLogs({
        address: "0x11f4d0A3c12e86B4b5F39B213F7E19D048276DAe",
        topics: ["0x033456732123ffff2342342dd12342434324234234fd234fd23fd4f23d4234"]
    })
    .then(console.log);

    > [{
        data: '0x7f9fade1c0d57a7af66ab4ead79fade1c0d57a7af66ab4ead7c2c2eb7b11a91385',
        topics: ['0xfd43ade1c09fade1c0d57a7af66ab4ead7c2c2eb7b11a91ffdd57a7af66ab4ead7', '0x7f9fade1c0d57a7af66ab4ead79fade1c0d57a7af66ab4ead7c2c2eb7b11a91385']
        logIndex: 0,
        transactionIndex: 0,
        transactionHash: '0x7f9fade1c0d57a7af66ab4ead79fade1c0d57a7af66ab4ead7c2c2eb7b11a91385',
        blockHash: '0xfd43ade1c09fade1c0d57a7af66ab4ead7c2c2eb7b11a91ffdd57a7af66ab4ead7',
        blockNumber: 1234,
        address: '0xde0B295669a9FD93d5F28D9Ec85E40f4cb697BAe'
    },{...}]


------------------------------------------------------------------------------