.. _apis-subscribe:

=========
app3.apis.subscribe
=========

The ``app3.apis.subscribe`` function lets you subscribe to specific events in the blockchain.



subscribe
=====================

.. code-block:: javascript

    app3.apis.subscribe(type [, options] [, callback]);

----------
Parameters
----------

1. ``String`` - The subscription, you want to subscribe to.
2. ``Mixed`` - (optional) Optional additional parameters, depending on the subscription type.
3. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second. Will be called for each incoming subscription, and the subscription itself as 3 parameter.

.. _apis-subscription-return:

-------
Returns
-------

``EventEmitter`` - A Subscription instance

    - ``subscription.id``: The subscription id, used to identify and unsubscribing the subscription.
    - ``subscription.subscribe([callback])``: Can be used to re-subscribe with the same parameters.
    - ``subscription.unsubscribe([callback])``: Unsubscribes the subscription and returns `TRUE` in the callback if successfull.
    - ``subscription.arguments``: The subscription arguments, used when re-subscribing.
    - ``on("data")`` returns ``Object``: Fires on each incoming log with the log object as argument.
    - ``on("changed")`` returns ``Object``: Fires on each log which was removed from the blockchain. The log will have the additional property ``"removed: true"``.
    - ``on("error")`` returns ``Object``: Fires when an error in the subscription occurs.

----------------
Notification returns
----------------

- ``Mixed`` - depends on the subscription, see the different subscriptions for more.

-------
Example
-------

.. code-block:: javascript

    var subscription = app3.apis.subscribe('logs', {
        address: '0x123456..',
        topics: ['0x12345...']
    }, function(error, result){
        if (!error)
            console.log(result);
    });

    // unsubscribes the subscription
    subscription.unsubscribe(function(error, success){
        if(success)
            console.log('Successfully unsubscribed!');
    });


------------------------------------------------------------------------------


clearSubscriptions
=====================

.. code-block:: javascript

    app3.apis.clearSubscriptions()

Resets subscriptions.

----------
Parameters
----------

1. ``Boolean``: If ``true`` it keeps the ``"syncing"`` subscription.

-------
Returns
-------

``Boolean``

-------
Example
-------

.. code-block:: javascript

    app3.apis.subscribe('logs', {} ,function(){ ... });

    ...

    app3.apis.clearSubscriptions();


------------------------------------------------------------------------------


subscribe("pendingTransactions")
=====================

.. code-block:: javascript

    app3.apis.subscribe('pendingTransactions' [, callback]);

Subscribes to incoming pending transactions.

----------
Parameters
----------

1. ``String`` - ``"pendingTransactions"``, the type of the subscription.
2. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second. Will be called for each incoming subscription.

-------
Returns
-------

``EventEmitter``: An :ref:`subscription instance <apis-subscription-return>` as an event emitter with the following events:

- ``"data"`` returns ``String``: Fires on each incoming pending transaction and returns the transaction hash.
- ``"error"`` returns ``Object``: Fires when an error in the subscription occurs.

----------------
Notification returns
----------------

1. ``Object|Null`` - First parameter is an error object if the subscription failed.
2. ``String`` - Second parameter is the transaction hash.

-------
Example
-------


.. code-block:: javascript

    var subscription = app3.apis.subscribe('pendingTransactions', function(error, result){
        if (!error)
            console.log(result);
    })
    .on("data", function(transaction){
        console.log(transaction);
    });

    // unsubscribes the subscription
    subscription.unsubscribe(function(error, success){
        if(success)
            console.log('Successfully unsubscribed!');
    });


------------------------------------------------------------------------------


subscribe("newBlockHeaders")
=====================

.. code-block:: javascript

    app3.apis.subscribe('newBlockHeaders' [, callback]);

Subscribes to incoming block headers. This can be used as timer to check for changes on the blockchain.

----------
Parameters
----------

1. ``String`` - ``"newBlockHeaders"``, the type of the subscription.
2. ``Function`` - (optional) Optional callback, returns an error object as first parameter and the result as second. Will be called for each incoming subscription.

-------
Returns
-------

``EventEmitter``: An :ref:`subscription instance <apis-subscription-return>` as an event emitter with the following events:

- ``"data"`` returns ``Object``: Fires on each incoming block header.
- ``"error"`` returns ``Object``: Fires when an error in the subscription occurs.

The structure of a returned block header is as follows:

    - ``number`` - ``Number``: The block number. ``null`` when its pending block.
    - ``hash`` 32 Bytes - ``String``: Hash of the block. ``null`` when its pending block.
    - ``parentHash`` 32 Bytes - ``String``: Hash of the parent block.
    - ``coinbase`` - ``String``: The address of the beneficiary to whom the mining rewards were given.
    - ``stateRoot`` 32 Bytes - ``String``: The root of the final state trie of the block.
    - ``txTrieHash`` 32 Bytes - ``String``: The root of the transaction trie of the block
    - ``receiptsTrieHash`` 32 Bytes - ``String``: The root of the receipts.
    - ``rewardPoint`` 32 Bytes - ``Number``: RewardPoint of coinbase for proof-of-stake
    - ``cumulativeRewardPoint`` 32 Bytes - ``Number``: RewardPoint accumulated up to the current block
    - ``gasLimit`` - ``Number``: The maximum gas allowed in this block.
    - ``gasUsed`` - ``Number``: The total used gas by all transactions in this block.
    - ``mineralUsed`` - ``Number``: The total used mineral by all transactions in this block.
    - ``timestamp`` - ``Number``: The unix timestamp for when the block was collated.
    - ``extraData`` - ``String``: The "extra data" field of this block.
    - ``logsBloom`` 256 Bytes - ``String``: The bloom filter for the logs of the block. ``null`` when its pending block.
    - ``rpSeed`` 32 Bytes - ``String``: Hash of the generated proof-of-stake. ``null`` when its pending block.
    - ``nonce`` 32 Bytes - ``String``: Balance of coinbase for proof-of-stake. ``null`` when its pending block.

----------------
Notification returns
----------------

1. ``Object|Null`` - First parameter is an error object if the subscription failed.
2. ``Object`` - The block header object like above.

-------
Example
-------


.. code-block:: javascript

    var subscription = app3.apis.subscribe('newBlockHeaders', function(error, result){
        if (!error) {
            console.log(result);

            return;
        }

        console.error(error);
    })
    .on("data", function(blockHeader){
        console.log(blockHeader);
    })
    .on("error", console.error);

    // unsubscribes the subscription
    subscription.unsubscribe(function(error, success){
        if (success) {
            console.log('Successfully unsubscribed!');
        }
    });

------------------------------------------------------------------------------


subscribe("logs")
=====================

.. code-block:: javascript

    app3.apis.subscribe('logs', options [, callback]);

Subscribes to incoming logs, filtered by the given options.

----------
Parameters
----------

1. ``"logs"`` - ``String``, the type of the subscription.
2. ``Object`` - The subscription options
  - ``fromBlock`` - ``Number``: The number of the earliest block. By default ``null``.
  - ``address`` - ``String|Array``: An address or a list of addresses to only get logs from particular account(s).
  - ``topics`` - ``Array``: An array of values which must each appear in the log entries. The order is important, if you want to leave topics out use ``null``, e.g. ``[null, '0x00...']``. You can also pass another array for each topic with options for that topic e.g. ``[null, ['option1', 'option2']]``
3. ``callback`` - ``Function``: (optional) Optional callback, returns an error object as first parameter and the result as second. Will be called for each incoming subscription.

-------
Returns
-------

``EventEmitter``: An :ref:`subscription instance <apis-subscription-return>` as an event emitter with the following events:

- ``"data"`` returns ``Object``: Fires on each incoming log with the log object as argument.
- ``"changed"`` returns ``Object``: Fires on each log which was removed from the blockchain. The log will have the additional property ``"removed: true"``.
- ``"error"`` returns ``Object``: Fires when an error in the subscription occurs.

For the structure of a returned event ``Object`` see :ref:`app3.apis.getPastEvents return values <apis-getpastlogs-return>`.

----------------
Notification returns
----------------

1. ``Object|Null`` - First parameter is an error object if the subscription failed.
2. ``Object`` - The log object like in :ref:`app3.apis.getPastEvents return values <apis-getpastlogs-return>`.

-------
Example
-------


.. code-block:: javascript

    var subscription = app3.apis.subscribe('logs', {
        address: '0x123456..',
        topics: ['0x12345...']
    }, function(error, result){
        if (!error)
            console.log(result);
    })
    .on("data", function(log){
        console.log(log);
    })
    .on("changed", function(log){
    });

    // unsubscribes the subscription
    subscription.unsubscribe(function(error, success){
        if(success)
            console.log('Successfully unsubscribed!');
    });