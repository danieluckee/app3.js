.. _apis:

.. include:: include_announcement.rst

========
API
========

Setting up APIS Core HTTP RPC
=======

You can run the HTTP RPC server through the APIS Core program under Linux.

For APIS Core installation instructions, please refer to the following `documentation <https://gist.github.com/Oxchild/9bd6f67a8d39ba9e2590b4f91f5b0450>`_

When you run APIS Core, the following configuration options are displayed on the screen.

.. image:: https://storage.googleapis.com/apis-mn-images/app3js/apis_core_setting.png

To activate the HTTP RPC server, press '8' on your keyboard and press 'Enter'.

When the setting of "RPC (HTTP) Enabled" item is changed to true, use of the HTTP RPC server is activated.

To access the server, keep the port number of [9], ID of [B], and Password of [C].

If you need to connect from external, set the value of [D] item to 0.0.0.0 or the IP address you want to connect.

Finally, press 'Enter' key to start the APIS Core program without entering anything.

Once the block synchronization starts, you can access the RPC server.

------------------------------------------------------------------------------



--------
Authenticating using a ID and Password
--------

As additional protection for your request traffic, you should use HTTP Basic Authentication to access API.

.. code-block:: console

    curl --user RPC-ID:RPC-PASSWORD http://127.0.0.1:45678



Securing Your Credentials
=======

--------
Authenticating using a ID and Password
--------

As additional protection for your request traffic, you should use HTTP Basic Authentication to access API.

.. code-block:: console

    curl --user RPC-ID:RPC-PASSWORD http://127.0.0.1:45678



Make Requests
=======

Below is a quick command line example using `curl`:

.. code-block:: console

    $ curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_blockNumber", "params": []}' \
        --user RPC-ID:RPC-Password \
        http://127.0.0.1:45678

The result should look something like this :

.. code-block:: console

    $ {"id":1,"jsonrpc":"2.0","method":"apis_blockNumber","result":"0x00000000001ea7eb"}

.. note:: "0x00000000001ea7eb" will be replaced with the block number of the most recent block on that network



------------------------------------------------------------------------------

apis_protocolVersion
=====================

Returns the APIS protocol version of the node

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_protocolVersion", "params": []}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String``: the protocol version.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_protocolVersion",
      "result": "2.0"
    }

------------------------------------------------------------------------------


apis_syncing
=====================

Checks if the node is currently syncing and returns either a syncing object, or ``false``.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_syncing", "params": []}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object|Boolean`` - A sync object when the node is currently syncing or ``false``:

    - ``startingBlock`` - ``Hex``: The block number where the sync started.
    - ``currentBlock`` - ``Hex``: The block number where at which block the node currently synced to already.
    - ``highestBlock`` - ``Hex``: The estimated block number to sync to.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_syncing",
      "result": {
        "startingBlock": "0x001E81C0",
        "currentBlock": "0x001EA894",
        "highestBlock": "0x001EA894"
      }
    }

------------------------------------------------------------------------------


apis_coinbase
=====================

Returns the coinbase address to which mining rewards will go.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_coinbase", "params": []}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678


-------
RESPONSE
-------

``result`` returns ``String`` - bytes 20 : The coinbase address set in the node for mining rewards.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_coinbase",
      "result": "0xbaf1abfc83c7f9594d12e101e93c71a3a6ec9fe9"
    }

------------------------------------------------------------------------------


apis_mining
=====================

Checks whapiser the node is mining or not.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_mining", "params": []}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Boolean``: ``true`` if the node is mining, otherwise ``false``.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_mining",
      "result": true
    }

------------------------------------------------------------------------------


apis_gasPrice
=====================

Returns the current gas price oracle.
The gas price is determined by the last few blocks median gas price.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_gasPrice", "params": []}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - Hex string of the current gas price in :ref:`wei <what-is-wei>`.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_gasPrice",
      "result": "0x0000000ba43b7400"
    }

------------------------------------------------------------------------------


apis_accounts
=====================

Returns a list of accounts the node controls.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_accounts", "params": []}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Array`` - Array of accounts controlled by node objects.

The structure of the returned account ``Object`` in the ``Array`` looks as follows:

- ``address`` 32 Bytes - ``String``: address of account
- ``index`` - ``Number``: The index position of accounts controlled by node.
- ``aAPIS`` - ``Number``: The current APIS balance for the given account in atto.
- ``aMNR`` - ``Number``: The current MNR balance for the given account in atto.
- ``nonce`` - ``Number``: The number of transactions
- ``APIS`` 32 Bytes - ``String``: The current APIS balance for the given account.
- ``MNR`` 32 Bytes - ``String``: The current MNR balance for the given account.
- ``proofKey`` 32 Bytes - ``String``: 2-Step Verification Key. ``null`` if not registered.
- ``isMasternode`` = ``Boolean``: True if given address is a masternode.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_accounts",
      "result": [
        {
          "address": "0xbaf1abfc83c7f9594d12e101e93c71a3a6ec9fe9",
          "index": "0",
          "aAPIS": "28008000000000",
          "aMNR": "0",
          "nonce": "2",
          "APIS": "0.000028008",
          "MNR": "0"
        }
      ]
    }

------------------------------------------------------------------------------


apis_getWalletInfo
=====================

Returns a information of given address.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_getWalletInfo", "params": ["c5f590c1035ae780906514ff8e76dd86b89b97dc"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_getWalletInfo", "params": ["some_name@me"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - Information of given address.

- ``address`` 32 Bytes - ``String``: address of account.
- ``mask`` - ``String``: Mask of given address. ``null`` if not registered.
- ``aAPIS`` - ``Number``: The current APIS balance for the given account in atto.
- ``aMNR`` - ``Number``: The current MNR balance for the given account in atto.
- ``nonce`` - ``Number``: The number of transactions
- ``APIS`` 32 Bytes - ``String``: The current APIS balance for the given account.
- ``MNR`` 32 Bytes - ``String``: The current MNR balance for the given account.
- ``proofKey`` 32 Bytes - ``String``: 2-Step Verification Key. ``null`` if not registered.
- ``isContract`` - ``String``: True if given address has contract code. ``null`` if hasn't.
- ``isMasternode`` = ``Boolean``: True if given address is a masternode.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getWalletInfo",
      "result": {
        "address": "f481dcace2750647fdad784b8981c3c16c1412cf",
        "mask": "some_name@me",
        "aAPIS": "0",
        "aMNR": "6480000000000000000",
        "nonce": "8",
        "APIS": "0",
        "MNR": "6.48",
        "Reward": "6,211.7274536893",
        "aReward": "6211727453689300000000"
      }
    }

------------------------------------------------------------------------------


apis_blockNumber
=====================

Returns the current block number.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_blockNumber", "params": []}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - Hex string of the most recent block.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_blockNumber",
      "result": "0x00000000001eac3e"
    }

------------------------------------------------------------------------------


apis_getBalance
=====================

Get the balance of an address.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_getBalance", "params": ["0xb29efff525b229b6efbe37979467e7c64b8a84ce","latest"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - The current balance for the given address in :ref:`atto <what-is-atto>`.

See the :ref:`A note on dealing with big numbers in JavaScript <big-numbers-in-javascript>`.

- ``aAPIS`` - ``Number``: The current APIS balance for the given account in atto.
- ``aMNR`` - ``Number``: The current MNR balance for the given account in atto.
- ``APIS`` - ``Number``: The current readable APIS balance for the given account.
- ``MNR`` - ``Number``: The current readable MNR balance for the given account.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getBalance",
      "result": {
        "aAPIS": "13248864789481155150000000",
        "aMNR": "926670950000000000",
        "APIS": "13,248,864.78948115515",
        "MNR": "0.92667095"
      }
    }

------------------------------------------------------------------------------


apis_getCode
=====================

Get the code at a specific address.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc": "2.0", "id": 1, "method": "apis_getCode", "params": ["866962b19d403a712f2c6bca390f9f295ba2dfe9"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - The data at given address ``address``.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getCode",
      "result": "0x608060405260043610610251576000357c0100000000000000000000000000000000000000000000..."
    }

------------------------------------------------------------------------------


apis_getBlockByNumber
=====================

Returns a block matching the block number.

-------
REQUEST
-------

1. ``Number`` - The block number.
2. ``Boolean`` - (optional, default ``false``) If ``true``, the returned block will contain all transactions as objects, if ``false`` it will only contains the transaction hashes.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getBlockByNumber","params":["0x2710",false]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - The block object:

  - ``number`` - ``Number``: The block number. ``null`` when its pending block.
  - ``hash`` 32 Bytes - ``String``: Hash of the block. ``null`` when its pending block.
  - ``parentHash`` 32 Bytes - ``String``: Hash of the parent block.
  - ``nonce`` - ``Number``: Balance of miner (10 blocks ago).
  - ``txTrieHash`` 32 Bytes - ``String``: The root of the transaction trie of the block
  - ``stateRoot`` 32 Bytes - ``String``: The root of the final state trie of the block.
  - ``coinbase`` - ``String``: The address of the beneficiary to whom the mining rewards were given.
  - ``coinbaseMask`` - ``String``: The mask of the coinbase.
  - ``rewardPoint`` - ``String``: Integer of the RP for this block of miner.
  - ``cumulativeRewardPoint`` - ``String``: Integer of the cumulative RP of the chain until this block.
  - ``extraData`` - ``String``: The "extra data" field of this block.
  - ``gasLimit`` - ``Number``: The maximum gas allowed in this block.
  - ``gasUsed`` - ``Number``: The total used gas by all transactions in this block.
  - ``mineralUsed`` - ``Number``: The total used mineral by all transactions in this block.
  - ``timestamp`` - ``Number``: The unix timestamp for when the block was collated.
  - ``transactions`` - ``Array``: Array of transaction objects, or 32 Bytes transaction hashes depending on the ``returnTransactionObjects`` parameter.
  - ``logsBloom`` 256 Bytes - ``String``: The bloom filter for the logs of the block. ``null`` when its pending block.
  - ``mnHash`` 32 Bytes - ``String``: Hash of the masternodes
  - ``mnReward`` - ``Number``: Base amount of Masternode rewards
  - ``mnGenerals`` - ``Array``: Array of general masternodes.
  - ``mnMajors`` - ``Array``: Array of major masternodes.
  - ``mnPrivates`` - ``Array``: Array of private masternodes.
  - ``size`` - ``Number``: Integer the size of this block in bytes.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getBlockByNumber",
      "result": {
        "number": 10000,
        "hash": "0x93c601f19b570fb689eceb6b1551b8cb7e8b6d1f6cfe980fd139496cedcd7a7e",
        "parentHash": "0xa89ad9435bcec17c3f8e399ec47afc7f98888179fd14abc54ea53df3c22d2833",
        "coinbase": "0x66a99a95246aa66237514f8aa03e2386351cf432",
        "coinbaseMask": "",
        "stateRoot": "0xdb9980ea807e0a7196f763cf52617b893a49f0529f7463f745ab3517b3dcfef3",
        "txTrieHash": "0xf1c97cbdc42aa950cdd248efecd218d58ca04455616e4134ef9a6cbe6e7b7c4a",
        "receiptsTrieHash": "0xffd51add8233eb9185f25c65f11b58c88035f39029c7fb1bbc95aa0d43a2198c",
        "rewardPoint": "1114791856847256934599402474396241043120000000000",
        "cumulativeRewardPoint": "8574244085992873522716157743293258609133280100000000",
        "gasLimit": 100000000,
        "gasUsed": 10347216,
        "mineralUsed": "10000000000000",
        "timestamp": "1546141681",
        "extraData": "0x4150495320706f776572656420536572766572",
        "rpSeed": "0x8af9dea17f02f394d3cd947f9f53e2e7c0a1fa348555a0a92aec9a4305b3046a",
        "nonce": "0x258dcd45652bd3d69000",
        "txSize": 11,
        "transactions": [
          "0x3235deae37b6aba20a56d1958394314803a84fab4d861f09677767bc2329a897",
          "0x5949bca2a8e9819834e85e14253d189e7e94d142880f9ebef086d3220d071d1e",
          "0xc9aa5ec867fbb47e44c75947bb5e4e57805f0810c9ad733302f52d548287aa8d",
          "0xcea711fe5000d2cd185757df6e8aa7b4f8d3858203d2d3aa97870b0e453f26e4",
          "0x9409ce9e169ff6007cbcaa3137d98bb385d3de1911d135544330cab903e863de",
          "0xd2006fc207f496c0f405744145caa82bf1f6fb44076c4f93583cc0029392baed",
          "0x76b02d08372af92f097f50b476d7984ea20da22a59af78e30fc85653a46e0dbb",
          "0x9c22b73a09cd05098c71faea4b688aa6b583d350b8a3a5c283377229e4bc9f5c",
          "0x084c5f07e5d4dc2fd101e275acdf71640d855bea7ea3ceeaf49aaab0797359db",
          "0x690a1a22262d18a3a0968bb367c270aa2ee5511917768f65eb6ac7e29e7b4bc2",
          "0x2f43ba31c6bbea3ecd08ad528344d1930bd82786cdff54b0c0df503f14a6987e"
        ],
        "logsBloom": "0x00000000000020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008000000000000000000080000000000000000000000000000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004002000000000000000000000000000000000000000000000000000000000000000000000000000",
        "size": 2695
      }
    }

------------------------------------------------------------------------------


apis_getBlockTransactionCountByNumber
=====================

Returns the number of transaction in a given block number.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getBlockTransactionCountByNumber","params":["0x2710"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Number`` - The number of transactions in the given block.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getBlockTransactionCountByNumber",
      "result": "0x0000000b"
    }

------------------------------------------------------------------------------


apis_getTransactionByHash
=====================

Returns a transaction matching the given transaction hash.

-------
REQUEST
-------

1. ``String`` - The transaction hash.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getTransactionByHash","params":["3235deae37b6aba20a56d1958394314803a84fab4d861f09677767bc2329a897"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - A transaction object, see :ref:`app3.apis.getTransaction <apis-gettransaction-return>`:

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getTransactionByHash",
      "result": {
        "hash": "0x3235deae37b6aba20a56d1958394314803a84fab4d861f09677767bc2329a897",
        "nonce": "0x11d1",
        "blockHash": "0x93c601f19b570fb689eceb6b1551b8cb7e8b6d1f6cfe980fd139496cedcd7a7e",
        "blockNumber": "10000",
        "transactionIndex": "0x0000000000000000",
        "timestamp": "1546141681",
        "from": "0xc5f590c1035ae780906514ff8e76dd86b89b97dc",
        "to": "0x866962b19d403a712f2c6bca390f9f295ba2dfe9",
        "value": "200000000000000000000000",
        "valueAPIS": "200,000",
        "gas": "4000000",
        "gasPrice": "50000000000",
        "gasPriceAPIS": "0.00000005",
        "feePaidAPIS": "0.2",
        "data": "0x2c32ac98000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc",
        "r": "0x62c934e772f9487e969464341f276ac83ee042d09f1ed569fe44aa78e3be0be2",
        "s": "0x38eff978bd0cc4b1116001422d0153546383f2966daa278ee11e9aade90e05ed",
        "v": "0x1c"
      }
    }

------------------------------------------------------------------------------


apis_getTransactionsByKeyword
=====================

Returns a transaction list matching the given keyword.

-------
REQUEST
-------

1. ``String`` - Keywords for retrieving transactions. ie. transaction hash, address, address mask.
2. ``String`` - Hex string of search results.
3. ``String`` - Hex string of skipped search results.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getTransactionsByKeyword","params":["platform@!","0x01","0x0"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Array`` - The list of transaction object, see : see :ref:`app3.apis.getTransaction <apis-gettransaction-return>`:

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getTransactionsByKeyword",
      "result": [
        {
          "status": "0x01",
          "transactionHash": "0xa6f58a7bfbb01c5bf36459b3822942e38a43fbdbc4c77a85b6fdb15e96a38aa8",
          "transactionIndex": 0,
          "blockHash": "0xac3eadc390d85c66b27a90ecdc6450a177d32c9302f13c3bcadecf258493",
          "blockNumber": 815509,
          "timestamp": "1553437790",
          "from": "0xf7f52b29fb123c56e79429b6a7e8797c64ce4b8a",
          "fromMask": "test@me",
          "to": "0x16bd780f5aff9a4f8eff003945fbd0251f5cf203",
          "value": "1230756000000000000000000",
          "valueAPIS": "1,230,756",
          "nonce": 5,
          "gas": 200000,
          "gasPrice": "50000000000",
          "gasPriceAPIS": "0.00000005",
          "gasUsed": 200000,
          "fee": "10000000000000000",
          "feeAPIS": "0.01",
          "mineralUsed": "10000000000000000",
          "mineralUsedMNR": "0.01",
          "feePaid": "0",
          "feePaidAPIS": "0",
          "cumulativeGasUsed": 200000,
          "cumulativeMineralUsed": "10000000000000000",
          "cumulativeMineralUsedMNR": "0.01",
          "data": ""
        }
      ]
    }

------------------------------------------------------------------------------


apis_getRecentTransactions
=====================

Returns a recent transaction list matching the given condition.

-------
REQUEST
-------

1. ``String`` - Hex string of search results. Default is 20
2. ``String`` - Hex string of skipped search results. Default is 0

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getRecentTransactions","params":["0x1","0x0"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Array`` - The list of transaction object, see : see :ref:`app3.apis.getTransaction <apis-gettransaction-return>`:

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getRecentTransactions",
      "result": [
        {
          "status": "0x01",
          "transactionHash": "0xc32eed1b70c5a17172214be87e5771fdc1c2ce0396e23b0c96f9323efffaa791",
          "blockHash": "0x04bad3183936ef3f0be6fad6f1b26ea53cb9bb1f0d31e935dd23cddc2ab5d299",
          "blockNumber": 1325386,
          "timestamp": "1556664769",
          "from": "0xf7afadb79a8b4579e3e113298eec866f7928c435",
          "to": "0xe6e11a5b9f794e398e7a832a764cb7728cfad359",
          "value": "1000000000000000000",
          "valueAPIS": "1.0",
          "gas": 220000,
          "gasPrice": "60000000000",
          "gasPriceAPIS": "0.00000006",
          "gasUsed": 213000,
          "fee": "12780000000000000",
          "feeAPIS": "0.01278",
          "mineralUsed": "12780000000000000",
          "mineralUsedMNR": "0.01278",
          "feePaid": "0",
          "feePaidAPIS": "0"
        }
      ]
    }

------------------------------------------------------------------------------


apis_getRecentBlocks
=====================

Returns a block list of recent confirmed.

-------
REQUEST
-------

1. ``String`` - Maximum hex string of search results. Default is 20
2. ``String`` - Hex string of skipped search results. Default is 1

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getRecentBlocks","params":["0x1","0x0"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Array`` - The list of block object:

  - ``number`` - ``Number``: The block number. ``null`` when its pending block.
  - ``hash`` 32 Bytes - ``String``: Hash of the block. ``null`` when its pending block.
  - ``parentHash`` 32 Bytes - ``String``: Hash of the parent block.
  - ``nonce`` - ``Number``: Balance of miner (10 blocks ago).
  - ``txTrieHash`` 32 Bytes - ``String``: The root of the transaction trie of the block
  - ``stateRoot`` 32 Bytes - ``String``: The root of the final state trie of the block.
  - ``coinbase`` - ``String``: The address of the beneficiary to whom the mining rewards were given.
  - ``coinbaseMask`` - ``String``: The mask of the coinbase.
  - ``rewardPoint`` - ``String``: Integer of the RP for this block of miner.
  - ``cumulativeRewardPoint`` - ``String``: Integer of the cumulative RP of the chain until this block.
  - ``extraData`` - ``String``: The "extra data" field of this block.
  - ``gasLimit`` - ``Number``: The maximum gas allowed in this block.
  - ``gasUsed`` - ``Number``: The total used gas by all transactions in this block.
  - ``mineralUsed`` - ``Number``: The total used mineral by all transactions in this block.
  - ``timestamp`` - ``Number``: The unix timestamp for when the block was collated.
  - ``transactions`` - ``Array``: Array of transaction objects, or 32 Bytes transaction hashes depending on the ``returnTransactionObjects`` parameter.
  - ``logsBloom`` 256 Bytes - ``String``: The bloom filter for the logs of the block. ``null`` when its pending block.
  - ``mnHash`` 32 Bytes - ``String``: Hash of the masternodes
  - ``mnReward`` - ``Number``: Base amount of Masternode rewards
  - ``mnGenerals`` - ``Array``: Array of general masternodes.
  - ``mnMajors`` - ``Array``: Array of major masternodes.
  - ``mnPrivates`` - ``Array``: Array of private masternodes.
  - ``size`` - ``Number``: Integer the size of this block in bytes.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getRecentBlocks",
      "result": [
        {
          "number": 2018136,
          "hash": "0xd4e19ade32e4391c32dfbc29ee8c87bc191d72ca904179f9402651790e00d3e3",
          "parentHash": "0x8ca6c4268fb54ef8acf53b944ea37c6c9f0af7214d2fd3d57c9312fde23a309d",
          "coinbase": "0x16b929423a857eebc802bb2b01a4a210998cfc29",
          "stateRoot": "0x16a70f060ba3d2f43c42146395d63b22c99c557dbd174cb2cb50d46f068ea002",
          "txTrieHash": "0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421",
          "receiptsTrieHash": "0x56e81f171bcc55a6ff8345e692c0f86e5b48e01b996cadc001622fb5e363b421",
          "rewardPoint": "191079955141446231419430344295608268161444500000000",
          "cumulativeRewardPoint": "1610721825131685775212700422192419886347044236464110977499",
          "gasLimit": 100000000,
          "gasUsed": 0,
          "mineralUsed": "0",
          "timestamp": "1562206769",
          "extraData": "0x4150495320706f776572656420536572766572",
          "rpSeed": "0x3aa9ba27dcbc9bb432e01bc09bf37f31179d4f89236b8c7883b9d7c13bd133b7",
          "nonce": "0x05f4f034afcf691f666d00",
          "txSize": 0,
          "transactions": [

          ],
          "size": 622
        }
      ]
    }

------------------------------------------------------------------------------


apis_getTransactionByBlockNumberAndIndex
=====================

Returns a transaction based on a block number and the transactions index position.

-------
REQUEST
-------

1. ``String`` - Hex string of block number.
2. ``String`` - Hex string of the transactions index position.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getTransactionByBlockNumberAndIndex","params":["0x2710","0x1"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - A transaction object, see :ref:`app3.apis.getTransaction <apis-gettransaction-return>`:

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getTransactionByBlockNumberAndIndex",
      "result": {
        "hash": "0x5949bca2a8e9819834e85e14253d189e7e94d142880f9ebef086d3220d071d1e",
        "nonce": "0x11d2",
        "blockHash": "0x93c601f19b570fb689eceb6b1551b8cb7e8b6d1f6cfe980fd139496cedcd7a7e",
        "blockNumber": "10000",
        "timestamp": "1546141681",
        "from": "0xc5f590c1035ae780906514ff8e76dd86b89b97dc",
        "to": "0x866962b19d403a712f2c6bca390f9f295ba2dfe9",
        "value": "200000000000000000000000",
        "valueAPIS": "200,000",
        "gas": "4000000",
        "gasPrice": "50000000000",
        "gasPriceAPIS": "0.00000005",
        "feePaidAPIS": "0.2",
        "data": "0x2c32ac98000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc",
        "r": "0xc83ef4698b1f8b91b3f6fcb0675cddd10cf292a2966a0f062008eac81ee653d9",
        "s": "0x1a4035422b21cde64902b993f7aec9bd23ec6180ebd34241e35f518c0dab6939",
        "v": "0x1b"
      }
    }

------------------------------------------------------------------------------


apis_getTransactionByBlockHashAndIndex
=====================

Returns a transaction based on a block hash and the transactions index position.

-------
REQUEST
-------

1. ``String`` - Hex string of block hash.
2. ``String`` - Hex string of the transactions index position.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getTransactionByBlockHashAndIndex","params":["0x93c601f19b570fb689eceb6b1551b8cb7e8b6d1f6cfe980fd139496cedcd7a7e","0x1"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - A transaction object, see :ref:`app3.apis.getTransaction <apis-gettransaction-return>`:

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getTransactionByBlockHashAndIndex",
      "result": {
        "hash": "0x5949bca2a8e9819834e85e14253d189e7e94d142880f9ebef086d3220d071d1e",
        "nonce": "0x11d2",
        "blockHash": "0x93c601f19b570fb689eceb6b1551b8cb7e8b6d1f6cfe980fd139496cedcd7a7e",
        "blockNumber": "10000",
        "timestamp": "1546141681",
        "from": "0xc5f590c1035ae780906514ff8e76dd86b89b97dc",
        "to": "0x866962b19d403a712f2c6bca390f9f295ba2dfe9",
        "value": "200000000000000000000000",
        "valueAPIS": "200,000",
        "gas": "4000000",
        "gasPrice": "50000000000",
        "gasPriceAPIS": "0.00000005",
        "feePaidAPIS": "0.2",
        "data": "0x2c32ac98000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc",
        "r": "0xc83ef4698b1f8b91b3f6fcb0675cddd10cf292a2966a0f062008eac81ee653d9",
        "s": "0x1a4035422b21cde64902b993f7aec9bd23ec6180ebd34241e35f518c0dab6939",
        "v": "0x1b"
      }
    }

------------------------------------------------------------------------------


apis_getTransactionReceipt
=====================

Returns the receipt of a transaction by transaction hash.

-------
REQUEST
-------

1. ``String`` - Hex string of block hash.
2. ``String`` - Hex string of the transactions index position.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getTransactionReceipt","params":["3235deae37b6aba20a56d1958394314803a84fab4d861f09677767bc2329a897"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - A transaction receipt object, or ``null`` when no receipt was found:

  - ``status`` - ``Boolean``: ``TRUE`` if the transaction was successful, ``FALSE``, if the EVM reverted the transaction.
  - ``blockHash`` 32 Bytes - ``String``: Hash of the block where this transaction was in.
  - ``blockNumber`` - ``Number``: Block number where this transaction was in.
  - ``transactionHash`` 32 Bytes - ``String``: Hash of the transaction.
  - ``transactionIndex``- ``Number``: Integer of the transactions index position in the block.
  - ``timestamp``- ``Number``: The unix timestamp for when the block was collated.
  - ``from`` - ``String``: Address of the sender.
  - ``to`` - ``String``: Address of the receiver. ``null`` when its a contract creation transaction.
  - ``toMask`` - ``String``: Mask of the receiver. ``null`` when its not registered.
  - ``contractAddress`` - ``String``: The contract address created, if the transaction was a contract creation, otherwise ``null``.
  - ``gas`` - ``Number``: Gas provided by the sender.
  - ``gasPrice`` - ``String``: Gas price provided by the sender in atto.
  - ``gasPriceAPIS`` - ``String``: Gas price in APIS.
  - ``gasUsed``- ``Number``:  The amount of gas used by this specific transaction alone.
  - ``mineralUsed`` - ``String``: The amount of mineral used by this specific transaction alone.
  - ``mineralUsedMNR`` - ``String``: Used mineral in MNR.
  - ``feePaid`` - ``String``: Finally paid fee amount in atto.
  - ``feePaidAPIS`` - ``String``: Paid fee amount in APIS.
  - ``cumulativeGasUsed`` - ``Number``: The total amount of gas used when this transaction was executed in the block.
  - ``cumulativeMineralUsed`` - ``Number``: The total amount of mineral used when this transaction was executed in the block.
  - ``cumulativeMineralUsedMNR`` - ``Number``: Cumulative mineral in MNR
  - ``logs`` - ``Array``: Array of log objects, which this transaction generated.
  - ``internalTransaction`` - ``Array``: Array of internal transaction objects.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getTransactionReceipt",
      "result": {
        "status": "0x01",
        "transactionHash": "0x3235deae37b6aba20a56d1958394314803a84fab4d861f09677767bc2329a897",
        "transactionIndex": 0,
        "blockHash": "0x93c601f19b570fb689eceb6b1551b8cb7e8b6d1f6cfe980fd139496cedcd7a7e",
        "blockNumber": 10000,
        "timestamp": "1546141681",
        "from": "0xc5f590c1035ae780906514ff8e76dd86b89b97dc",
        "to": "0x866962b19d403a712f2c6bca390f9f295ba2dfe9",
        "toMask": "platform@!",
        "value": "200000000000000000000000",
        "valueAPIS": "200,000",
        "nonce": 4561,
        "gas": 4000000,
        "gasPrice": "50000000000",
        "gasPriceAPIS": "0.00000005",
        "gasUsed": 940656,
        "fee": "47032800000000000",
        "feeAPIS": "0.0470328",
        "mineralUsed": "10000000000000",
        "mineralUsedMNR": "0.00001",
        "feePaid": "47022800000000000",
        "feePaidAPIS": "0.0470228",
        "cumulativeGasUsed": 940656,
        "cumulativeMineralUsed": "10000000000000",
        "cumulativeMineralUsedMNR": "0.00001",
        "logs": [
          {
            "address": "0x866962b19d403a712f2c6bca390f9f295ba2dfe9",
            "topics": [
              "0xb78b065b331993702eab550134725ee93e37c65976734cd4317b7df694986489"
            ],
            "data": "0x000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc0000000000000000000000005d4084816b42dc5b19ceb9ced86e82a2c37e9b38000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc000000000000000000000000000000000000000000002a5a058fc295ed000000",
            "transactionHash": "0x3235deae37b6aba20a56d1958394314803a84fab4d861f09677767bc2329a897",
            "logIndex": "0x",
            "blockNumber": 0,
            "transactionIndex": 0
          }
        ],
        "logsBloom": "0x00000000000020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008000000000000000000080000000000000000000000000000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004002000000000000000000000000000000000000000000000000000000000000000000000000000",
        "data": "0x2c32ac98000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc"
      }
    }

------------------------------------------------------------------------------


apis_getTransactionCount
=====================

Get the numbers of transactions sent from this address.

-------
REQUEST
-------

``String`` - The address to get the numbers of transactions from.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getTransactionCount","params":["0xc5f590c1035ae780906514ff8e76dd86b89b97dc"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Number`` - The number of transactions sent from the given address.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getTransactionCount",
      "result": "0x0086d4"
    }

------------------------------------------------------------------------------


apis_getMasternodeCount
=====================

Get the number of masternodes.

Masternode State :
  - Earlybird - Masternode that can be joined through apis.mn. The nodes are joined to the day(10,800 blocks) before the round begins.
  - Normal - Masternode joined through APIS Core within the first day(10,800 blocks) of the round
  - Late - Masternode joined through the APIS Core after the normal participation period of the round

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getMasternodeCount","params":[]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - The number of masternodes.

  - ``generalEarly`` - ``Number``: Number of General Earlybird Masternodes
  - ``majorEarly`` - ``Number``: Number of Major Earlybird Masternodes
  - ``privateEarly`` - ``Number``: Number of Private Earlybird Masternodes
  - ``generalNormal`` - ``Number``: Number of General Normal Masternodes
  - ``majorNormal`` - ``Number``: Number of Major Normal Masternodes
  - ``privateNormal`` - ``Number``: Number of Private Normal Masternodes
  - ``generalLate`` - ``Number``: Number of General Late Masternodes
  - ``majorLate`` - ``Number``: Number of Major Late Masternodes
  - ``privateLate`` - ``Number``: Number of Private Late Masternodes

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getMasternodeCount",
      "result": {
        "generalEarly": 3390,
        "majorEarly": 2907,
        "privateEarly": 1967,
        "generalNormal": 0,
        "majorNormal": 0,
        "privateNormal": 2,
        "generalLate": 1,
        "majorLate": 0,
        "privateLate": 2
      }
    }

------------------------------------------------------------------------------


apis_sendRawTransaction
=====================

Sends an already signed transaction.

-------
REQUEST
-------

1. ``String`` - Signed transaction data in HEX format

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":4,"method":"apis_sendRawTransaction","params":["0xf87380850ba43b740083030d40943535353535353535353535353535353535353535808a152d02c7e14af68000008026a09af43e6be42c3c4286a2f922401e7a0fb3414d9492609ad1fff08aec6fb24953a064b4a0545ca8c56f3679a9df3eaaff2f81fc8a39089213908a70b47480d24d59808080"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - The transaction hash

.. code-block:: console

    {
      "id": 4,
      "jsonrpc": "2.0",
      "method": "apis_sendRawTransaction",
      "result": "0xac05c474f7ff71cd8d91169d41e0efb92a5cbc7f9ec1bdb3ba8ec1c3d6c14cd5"
    }

------------------------------------------------------------------------------


apis_sendTransaction
=====================

Sends a transaction to the network. The sender address must be unlocked.

-------
REQUEST
-------

1. ``Object`` - The transaction object to send:
  - ``from`` - ``String``: The address for the sending account.
  - ``to`` - ``String``: (optional) The destination address of the message, left undefined for a contract-creation transaction.
  - ``value`` - ``String``: (optional) The value transferred for the transaction in :ref:`atto <what-is-atto>`, also the endowment if it's a contract-creation transaction in hex format.
  - ``gas``  - ``String``: (optional, default: To-Be-Determined) The amount of gas to use for the transaction (unused gas is refunded) in hex format.
  - ``gasPrice`` - ``String``: (optional) The price of gas in hex format for this transaction in :ref:`atto <what-is-atto>` , defaults to :ref:`app3.apis.gasPrice <apis-gasprice>`.
  - ``data`` - ``String``: (optional) Either a `ABI byte string <http://solidity.readthedocs.io/en/latest/abi-spec.html>`_ containing the data of the function call on a contract, or in the case of a contract-creation transaction the initialisation code.
  - ``nonce`` - ``String``: (optional) Hex string of a nonce. This allows to overwrite your own pending transactions that use the same nonce.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":3,"method":"apis_sendTransaction","params":[{"from":"0x71f81feeb2bc39456455e4793c3ed9286132f56b","gas":"0x30d40","to":"0x3535353535353535353535353535353535353535","value":"0x0","gasPrice":"0x0000000ba43b7400"}]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - The transaction hash

.. code-block:: console

    {
      "id": 3,
      "jsonrpc": "2.0",
      "method": "apis_sendTransaction",
      "error": {
        "code": -1,
        "message": "Not enough cash: Require: 10000000000000000, Sender cash: 0"
      }
    }

    {
      "id": 3,
      "jsonrpc": "2.0",
      "method": "apis_sendTransaction",
      "error": {
        "code": -1,
        "message": "Sender account is locked"
      }
    }

    {
      "id": 3,
      "jsonrpc": "2.0",
      "method": "apis_sendTransaction",
      "result": "0xac05c474f7ff71cd8d91169d41e0efb92a5cbc7f9ec1bdb3ba8ec1c3d6c14cd5"
    }

------------------------------------------------------------------------------


apis_sign
=====================

Signs data using a specific account. This account needs to be unlocked.

-------
REQUEST
-------

1. ``String`` - Data to sign in Hex format. The String can be converted using :ref:`app3.utils.utf8ToHex <utils-utf8tohex>`.
2. ``String`` - Address to sign data with.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":2,"method":"apis_sign","params":["0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01","0x48656c6c6f20776f726c64"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - The signature.

.. code-block:: console

    {
      "id": 2,
      "jsonrpc": "2.0",
      "method": "apis_sign",
      "result": "0xf4870babd44ad7b26f99ff1d8249d9aeb8aabe7585f81cd8bed15c04c1aafd28163e6a418ccc92544c4257da634f787aeb6292b611800861e17909e3d9e6820b01"
    }

------------------------------------------------------------------------------


apis_call
=====================

Executes a message call transaction, which is directly executed in the VM of the node, but never mined into the blockchain.

-------
REQUEST
-------

1. ``Object`` - A transaction object see :ref:`app3.apis.sendTransaction <apis-sendtransaction-return>`, with the difference that for calls the ``from`` property is optional as well.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_call","params":[{"to":"0x866962b19d403a712f2c6bca390f9f295ba2dfe9","data":"0x2fc1f1900000000000000000000000000000000000000000000000000000000000000000"},"latest"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String``: The returned data of the call, e.g. a smart contract functions return value.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_call",
      "result": "0x000000000000000000000000c5f590c1035ae780906514ff8e76dd86b89b97dc"
    }

------------------------------------------------------------------------------


apis_estimateGas
=====================

Executes a message call transaction, which is directly executed in the VM of the node, but never mined into the blockchain.

-------
REQUEST
-------

1. ``Object`` - A transaction object see :ref:`app3.apis.sendTransaction <apis-sendtransaction-return>`, with the difference that for calls the ``from`` property is optional as well.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":2,"method":"apis_estimateGas","params":[{"from":"0x71f81feeb2bc39456455e4793c3ed9286132f56b","gas":"0x30d40","to":"0x3535353535353535353535353535353535353535","value":"0x0","keystorepassword":"pA$sw0rD"}]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Number`` - the used gas for the simulated call/transaction.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_estimateGas",
      "result": "0x0000000000030d40"
    }

------------------------------------------------------------------------------


apis_getLogs
=====================

Gets past logs, matching the given options.

-------
REQUEST
-------

1. ``Object`` - The filter options as follows:
  - ``fromBlock`` - ``String``: Hex string of the earliest block
  - ``toBlock`` -  ``String``: Hex string of the latest block
  - ``address`` -  ``String|Array``: An address or a list of addresses to only get logs from particular account(s).
  - ``topics`` - ``Array``: An array of values which must each appear in the log entries. The order is important, if you want to leave topics out use ``null``, e.g. ``[null, '0x12...']``. You can also pass an array for each topic with options for that topic e.g. ``[null, ['option1', 'option2']]``

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"apis_getLogs","params":[{"fromBlock":"0x1e8480","address":"0x866962b19d403a712f2c6bca390f9f295ba2dfe9","topics":[]}]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

`result`` returns ``Array`` - Array of log objects.

The structure of the returned event ``Object`` in the ``Array`` looks as follows:

- ``address`` - ``String``: From which this event originated from.
- ``data`` - ``String``: The data containing non-indexed log parameter.
- ``topics`` - ``Array``: An array with max 4 32 Byte topics, topic 1-3 contains indexed parameters of the log.
- ``logIndex`` - ``Number``: Integer of the event index position in the block.
- ``transactionIndex`` - ``Number``: Integer of the transaction's index position, the event was created in.
- ``transactionHash`` 32 Bytes - ``String``: Hash of the transaction this event was created in.
- ``blockHash`` 32 Bytes - ``String``: Hash of the block where this event was created in. ``null`` when its still pending.
- ``blockNumber`` - ``Number``: The block number where this log was created in. ``null`` when still pending.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "apis_getLogs",
      "result": [
        {
          "status": "0x01",
          "transactionHash": "0x5168279997a492bb89dac17409f74709f3def7439c8460d69bf0ed28ebeabc81",
          "transactionIndex": 0,
          "blockHash": "0x64e4f0b3191012e40f4e89f1e1b5beec98fa9480db1ae241288e4ef99b4f1e61",
          "blockNumber": 2000077,
          "timestamp": "1562062297",
          "from": "0xc5f590c1035ae780906514ff8e76dd86b89b97dc",
          "to": "0x866962b19d403a712f2c6bca390f9f295ba2dfe9",
          "value": "0",
          "valueAPIS": "0",
          "nonce": 34499,
          "gas": 4000000,
          "gasPrice": "50000000000",
          "gasPriceAPIS": "0.00000005",
          "gasUsed": 347670,
          "fee": "17383500000000000",
          "feeAPIS": "0.0173835",
          "mineralUsed": "1600000000000000",
          "mineralUsedMNR": "0.0016",
          "feePaid": "15783500000000000",
          "feePaidAPIS": "0.0157835",
          "cumulativeGasUsed": 347670,
          "cumulativeMineralUsed": "1600000000000000",
          "cumulativeMineralUsedMNR": "0.0016",
          "logs": [
            {
              "address": "0x866962b19d403a712f2c6bca390f9f295ba2dfe9",
              "topics": [
                "0x19b6dcee97bdeda88d78fa94793f2271c3d9eceba8f8c02a04bff5d632ca3742"
              ],
              "data": "0x0000000000000000000000005527867bad124260b636c6f5991147e18ddf6ec10000000000000000000000000f6d8cf6194972fe31d047631a12977f899e4cfa000000000000000000000000000000000000000000000a968163f0a57b400000",
              "transactionHash": "0x5168279997a492bb89dac17409f74709f3def7439c8460d69bf0ed28ebeabc81",
              "logIndex": "0x",
              "blockNumber": 0,
              "transactionIndex": 0
            }
          ],
          "logsBloom": "0x00000000000020000000010000000000000020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004000000000000000000000002000000000000000000000000000000000000000000000000000000000000000000000000000",
          "data": "0x0c05f1890000000000000000000000000f6d8cf6194972fe31d047631a12977f899e4cfa0000000000000000000000000000000000000000000000000000000000000000"
        }
      ]
    }

------------------------------------------------------------------------------


apis_call
=====================

Create a new account on the node that App3js is connected to with its provider. The RPC method used is ``personal_newAccount``. It differs from :ref:`app3.apis.accounts.create() <apis-accounts-create>` where the key pair is created only on client and it’s up to the developer to manage it.

.. note:: Never call this function over a unsecured Websocket or HTTP provider, as your password will be send in plain text!

-------
REQUEST
-------

1. ``password`` - ``String``: The password to encrypt this account with.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"personal_newAccount","params":["pAs$w0Rd"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String``: The address of the newly created account.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "personal_newAccount",
      "result": "0xd174cf67355c1df76c18049d5f08637c32836387"
    }

------------------------------------------------------------------------------


personal_sign
=====================

Signs data using a specific account.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.

-------
REQUEST
-------

1. ``String`` - Data to sign in Hex format. The String can be converted using :ref:`app3.utils.utf8ToHex <utils-utf8tohex>`.
2. ``String`` - Address to sign data with.
3. ``String`` - The password of the account to sign data with.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"personal_sign","params":["0x48656c6c6f20776f726c64","0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01","pAs$w0Rd"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - The signature.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "personal_sign",
      "result": "0xf4870babd44ad7b26f99ff1d8249d9aeb8aabe7585f81cd8bed15c04c1aafd28163e6a418ccc92544c4257da634f787aeb6292b611800861e17909e3d9e6820b01"
    }

------------------------------------------------------------------------------


personal_ecRecover
=====================

Recovers the account that signed the data.

-------
REQUEST
-------

1. ``String`` - Data that was signed. If String it will be converted using :ref:`app3.utils.utf8ToHex <utils-utf8tohex>`.
2. ``String`` - The signature in Hex format.
3. ``String`` - The password of the account to sign data with.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"personal_ecRecover","params":["0x48656c6c6f20776f726c64","f4870babd44ad7b26f99ff1d8249d9aeb8aabe7585f81cd8bed15c04c1aafd28163e6a418ccc92544c4257da634f787aeb6292b611800861e17909e3d9e6820b01"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - The account.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "personal_ecRecover",
      "result": "0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01"
    }

------------------------------------------------------------------------------


personal_signTransaction
=====================

Signs a transaction. This account needs to be unlocked.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.

-------
REQUEST
-------

1. ``Object`` - The transaction data to sign :ref:`app3.apis.sendTransaction() <apis-sendtransaction>` for more.
2. ``String`` - The password of the ``from`` account, to sign the transaction with.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"personal_signTransaction","params":[{"from":"0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01","gasPrice":"0xba43b7400","gas":"0x30d40","to":"0x3535353535353535353535353535353535353535","value":"0xde0b6b3a7640000","data":""},"pAs$w0Rd"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - The RLP encoded transaction. The ``raw`` property can be used to send the transaction using :ref:`app3.apis.sendSignedTransaction <apis-sendsignedtransaction>`.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "personal_signTransaction",
      "result": {
        "raw": "0xf87180850ba43b740083030d4094353535353535353535353535353535353535353580880de0b6b3a76400008025a00975caffb4cb5c6f9dfe97315d3c448926ee0d3be85a44c1006be525d38ef5a3a01b8c0d9ac65615823dbdc0dfd398f5807d0582f490b2b408224e4803d46c3b69018080",
        "tx": {
          "hash": "0x185da901f30171b573d06ffdf1cbe8aa7e624eb3d207e4c6cede5c28c0fd948c",
          "nonce": "0x",
          "from": "0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01",
          "to": "0x3535353535353535353535353535353535353535",
          "value": "1000000000000000000",
          "valueAPIS": "1",
          "gas": "200000",
          "gasPrice": "50000000000",
          "gasPriceAPIS": "0.00000005",
          "feePaidAPIS": "0.01",
          "data": "",
          "r": "0x0975caffb4cb5c6f9dfe97315d3c448926ee0d3be85a44c1006be525d38ef5a3",
          "s": "0x1b8c0d9ac65615823dbdc0dfd398f5807d0582f490b2b408224e4803d46c3b69",
          "v": "0x1b"
        }
      }
    }

------------------------------------------------------------------------------


personal_unlockAccount
=====================

Unlocks the given account.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.

-------
REQUEST
-------

1. ``address`` - ``String`` - The account address.
2. ``password`` - ``String`` - The password of the account.
3. ``unlockDuration`` - ``Number`` - The duration seconds for the account to remain unlocked.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"personal_unlockAccount","params":["0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01","pAs$w0Rd",600]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Object`` - The RLP encoded transaction. The ``raw`` property can be used to send the transaction using :ref:`app3.apis.sendSignedTransaction <apis-sendsignedtransaction>`.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "personal_unlockAccount",
      "result": true
    }

------------------------------------------------------------------------------


personal_lockAccount
=====================

Locks the given account.

-------
REQUEST
-------

1. ``address`` - ``String`` - The account address.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"personal_lockAccount","params":["0xb8ce9ab6943e0eced004cde8e3bbed6568b2fa01"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``boolean`` - True if the account got locked successful otherwise false.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "personal_lockAccount",
      "result": true
    }

------------------------------------------------------------------------------


personal_listAccounts
=====================

Returns a list of accounts the node controls by using the provider and calling the RPC method ``personal_listAccounts``.

The results are the same as RPC method ``apis_accounts``.

-------
REQUEST
-------

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"personal_listAccounts","params":[]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``Array`` - An array of addresses controlled by node.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "personal_listAccounts",
      "result": [
        {
          "address": "aaad4d1b7bcb7ac91f33ab75027257562ac06b5e",
          "index": "0",
          "aAPIS": "28008000000000",
          "aMNR": "0",
          "nonce": "0",
          "APIS": "0.000028008",
          "MNR": "0"
        },
        {
          "address": "de9e9e4c0b94469ce9913cb040ec3d8f38bbdb64",
          "index": "1",
          "aAPIS": "5000000000000000000000",
          "aMNR": "20000000000000000",
          "nonce": "0",
          "APIS": "5,000",
          "MNR": "0.02"
        }
      ]
    }

------------------------------------------------------------------------------


personal_importRawKey
=====================

Imports the given private key into the key store, encrypting it with the passphrase.

Returns the address of the new account.

.. note:: Sending your account password over an unsecured HTTP RPC connection is highly unsecure.

-------
REQUEST
-------

1. ``privateKey`` - ``String`` - An unencrypted private key (hex string).
2. ``password`` - ``String`` - The password of the account.

.. code-block:: console

    curl -X POST \
        -H "Content-Type: application/json" \
        --data '{"jsonrpc":"2.0","id":1,"method":"personal_importRawKey","params":["0x348ce564d427a3311b6536bbcff9390d69395b06ed6c486954e971d960fe870c","aaaa"]}' \
        --user RPC-ID:RPC-Password http://127.0.0.1:45678

-------
RESPONSE
-------

``result`` returns ``String`` - The address of the account.

.. code-block:: console

    {
      "id": 1,
      "jsonrpc": "2.0",
      "method": "personal_importRawKey",
      "result": "9d158a9dc16c2c03aec4e6cbc8d29d7734a8b455"
    }

------------------------------------------------------------------------------

















