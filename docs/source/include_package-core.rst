

setProvider
=====================

.. code-block:: javascript

    app3.setProvider(myProvider)
    app3.apis.setProvider(myProvider)
    ...

Will change the provider for its module.

.. note:: When called on the umbrella package ``app3`` it will also set the provider for all sub modules ``web3.apis``, ``web3.shh``, etc which needs a separate provider at all times.

----------
Parameters
----------

1. ``Object`` - ``myProvider``: :ref:`a valid provider <app3-providers>`.

-------
Returns
-------

``Boolean``

-------
Example
-------

.. code-block:: javascript

    var App3 = require('app3js');

    app3.setProvider('ws://localhost:8546');
    // or
    app3.setProvider(new App3.providers.WebsocketProvider('ws://localhost:8546'));


------------------------------------------------------------------------------

providers
=====================

.. code-block:: javascript

    app3.providers
    app3.apis.providers
    ...

Contains the current available :ref:`providers <app3-providers>`.

----------
Value
----------

``Object`` with the following providers:

    - ``Object`` - ``WebsocketProvider``: The Websocket provider is the standard for usage in legacy browsers.

-------
Example
-------

.. code-block:: javascript

    var App3 = require('app3');
    var app3 = new App3('ws://remotenode.com:8546');
    // or
    var app3 = new App3(new App3.providers.WebsocketProvider('ws://remotenode.com:8546'));

------------------------------------------------------------------------------

givenProvider
=====================

.. code-block:: javascript

    app3.givenProvider
    app3.apis.givenProvider
    ...

When using app3js in an APIS compatible browser, it will set with the current native provider by that browser.
Will return the given provider by the (browser) environment, otherwise ``null``.


-------
Returns
-------

``Object``: The given provider set or ``null``;

-------
Example
-------

.. code-block:: javascript
    app3.setProvider(app3.givenProvider || "ws://remotenode.com:8546");

------------------------------------------------------------------------------


currentProvider
=====================

.. code-block:: javascript

    app3.currentProvider
    app3.apis.currentProvider
    ...

Will return the current provider, otherwise ``null``.


-------
Returns
-------

``Object``: The current provider set or ``null``;

-------
Example
-------

.. code-block:: javascript
    if(!app3.currentProvider) {
        app3.setProvider("ws://localhost:8545");
    }

------------------------------------------------------------------------------

BatchRequest
=====================

.. code-block:: javascript

    new app3.BatchRequest()
    new app3.apis.BatchRequest()

Class to create and execute batch requests.

----------
Parameters
----------

none

-------
Returns
-------

``Object``: With the following methods:

    - ``add(request)``: To add a request object to the batch call.
    - ``execute()``: Will execute the batch request.

-------
Example
-------

.. code-block:: javascript

    var contract = new app3.apis.Contract(abi, address);

    var batch = new app3.BatchRequest();
    batch.add(app3.apis.getBalance.request('0x0000000000000000000000000000000000000000', 'latest', callback));
    batch.add(contract.methods.balance(address).call.request({from: '0x0000000000000000000000000000000000000000'}, callback2));
    batch.execute();


------------------------------------------------------------------------------

extend
=====================

.. code-block:: javascript

    app3.extend(methods)
    app3.apis.extend(methods)
    ...

Allows extending the app3 modules.

.. note:: You also have ``*.extend.formatters`` as additional formatter functions to be used for in and output formatting.

----------
Parameters
----------

1. ``methods`` - ``Object``: Extension object with array of methods description objects as follows:
    - ``property`` - ``String``: (optional) The name of the property to add to the module. If no property is set it will be added to the module directly.
    - ``methods`` - ``Array``: The array of method descriptions:
        - ``name`` - ``String``: Name of the method to add.
        - ``call`` - ``String``: The RPC method name.
        - ``params`` - ``Number``: (optional) The number of parameters for that function. Default 0.
        - ``inputFormatter`` - ``Array``: (optional) Array of inputformatter functions. Each array item responds to a function parameter, so if you want some parameters not to be formatted, add a ``null`` instead.
        - ``outputFormatter - ``Function``: (optional) Can be used to format the output of the method.


----------
Returns
----------

``Object``: The extended module.

-------
Example
-------

.. code-block:: javascript

    app3.extend({
        property: 'myModule',
        methods: [{
            name: 'getBalance',
            call: 'apis_getBalance',
            params: 2,
            inputFormatter: [app3.extend.formatters.inputAddressFormatter, app3.extend.formatters.inputDefaultBlockNumberFormatter],
            outputFormatter: app3.utils.hexToNumberString
        },{
            name: 'getGasPriceSuperFunction',
            call: 'apis_gasPriceSuper',
            params: 2,
            inputFormatter: [null, app3.utils.numberToHex]
        }]
    });

    app3.extend({
        methods: [{
            name: 'directCall',
            call: 'apis_callForFun',
        }]
    });

    console.log(app3);
    > App3 {
        myModule: {
            getBalance: function(){},
            getGasPriceSuperFunction: function(){}
        },
        directCall: function(){},
        ...
    }


------------------------------------------------------------------------------